import mysql.connector
from mysql.connector import Error

try:
    # Establish the connection
    connection = mysql.connector.connect(
        #host='23.235.205.29',        # Example: 'example.com'
        host='23.235.205.29',
        database='finelo6_v5_db',
        user='finelo6_v5_user',
        password='GYS~0}XLkVbx'
    )

    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()

        # # Query to list all tables
        # cursor.execute("SHOW TABLES;")
        # tables = cursor.fetchall()

        # print("Tables in the database:")
        # for table in tables:
        #     print(table[0])
        
        
             # Execute the SQL statement
        # cursor.execute("""
        #     SELECT 
        #         rd.iDriverId,
        #         rd.vEmail,
        #         rd.tRegistrationDate,
        #         rd.vPhone,
        #         rd.vCode,
        #         rd.eStatus,
        #         rd.eIsFeatured,
        #         (
        #             SELECT 
        #                 COUNT(dv.iDriverVehicleId) 
        #             FROM 
        #                 driver_vehicle AS dv 
        #             WHERE 
        #                 dv.iDriverId=rd.iDriverId 
        #                 AND dv.eStatus != 'Deleted' 
        #                 AND dv.iMakeId != 0 
        #                 AND dv.iModelId != 0 
        #                 AND dv.eType != 'UberX'
        #         ) AS `count`,
        #         CONCAT(rd.vName,' ',rd.vLastName) AS driverName
        #     FROM 
        #         register_driver rd
        #     WHERE 
        #         1=1
        # """)
        
        
        cursor.execute("""
            SELECT 
                rd.iDriverId,
                rd.vEmail,
                rd.tRegistrationDate,
                rd.vPhone,
                rd.vCode,
                rd.eStatus,
                rd.eIsFeatured,
                (
                    SELECT 
                        COUNT(dv.iDriverVehicleId) 
                    FROM 
                        driver_vehicle AS dv 
                    WHERE 
                        dv.iDriverId=rd.iDriverId 
                        AND dv.eStatus != 'Deleted' 
                        AND dv.iMakeId != 0 
                        AND dv.iModelId != 0 
                        AND dv.eType != 'UberX'
                ) AS `count`,
                CONCAT(rd.vName,' ',rd.vLastName) AS driverName
            FROM 
                register_driver rd
            WHERE 
                1=1
        """)
        # Fetch the results
        results = cursor.fetchall()

        # Print the results
        for row in results:
            print(row)

except Error as e:
    print("Error while connecting to MySQL", e)

finally:
    if connection.is_connected():
        cursor.close()
        connection.close()
        print("MySQL connection is closed")
