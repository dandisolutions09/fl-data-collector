import mysql.connector
from mysql.connector import Error

try:
    # Establish the connection
    connection = mysql.connector.connect(
        #host='23.235.205.29',        # Example: 'example.com'
        host='23.235.205.29',
        database='finelo6_v5_db',
        user='finelo6_v5_user',
        password='GYS~0}XLkVbx'
    )

    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()
        
        #driver_id = 2001  # Replace with the driver ID you want to query
        driver_id = 2100

        
        #query = "SELECT * FROM `driver_vehicle` dv LEFT JOIN `service_pro_amount` spa ON dv.iDriverId = spa.iDriverVehicleId WHERE dv.`iDriverId` = %s"
        query = """
                SELECT * 
                FROM `driver_vehicle` dv 
                LEFT JOIN `service_pro_amount` spa ON dv.iDriverId = spa.iDriverVehicleId 
                WHERE dv.`iDriverId` = %s
            """
        
        cursor.execute(query, (driver_id,))
        
        
        driver_services = cursor.fetchone()
        
        print("driver services", driver_services)
        
        
                # Check if the 'fAmount' field is not empty
        # services_found = False
        # if driver_services and driver_services.get('fAmount') != '':
        #     services_found = True

        # Close cursor and connection
        cursor.close()
        connection.close()



except Error as e:
    print("Error while connecting to MySQL", e)

finally:
    if connection.is_connected():
        cursor.close()
        connection.close()
        print("MySQL connection is closed")
