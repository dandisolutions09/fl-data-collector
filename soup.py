from bs4 import BeautifulSoup
import csv


html_content = """

        <!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEAD-->
    <head>
        <meta charset="UTF-8" />
        <title>FineLooks |  Service Edit</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="keywords" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link href="../assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
        <!--[if IE]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->
<!-- GLOBAL STYLES -->
<link rel="icon" href="../favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="../assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="css/main.css" />
<link rel="stylesheet" href="../assets/css/theme.css" />
<link rel="stylesheet" href="../assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="../assets/plugins/Font-Awesome/css/font-awesome.css" />
<link rel="stylesheet" href="../assets/plugins/font-awesome-4.6.3/css/font-awesome.min.css" />
<link rel="stylesheet" href="css/style.css" />

<script src="../assets/plugins/jquery-2.0.3.min.js"></script> 
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<!-- END PAGE LEVEL  STYLES -->
   <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<script>
    function checkAlls() {
        jQuery("#_list_form input[type=checkbox]").each(function () {
            if ($(this).attr('disabled') != 'disabled') {
                this.checked = 'true';
            }
        });
    }

    function uncheckAlls() {
        jQuery("#_list_form input[type=checkbox]").each(function () {
            this.checked = '';
        });
    }

    function ChangeStatusAll(statusNew) {
        if (statusNew != "") {
            var status = statusNew;
            var checked = $("#_list_form input:checked").length;
            if (checked > 0) {
                if (checked == 1) {
                    if ($("#_list_form input:checked").attr("id") == 'setAllCheck') {
                        $('#is_not_check_modal').modal('show');
                        $("#changeStatus").val('');
                        return false;
                    }
                }
                $("#statusVal").val(status);
                if (status == 'Active') {
                    $('#is_actall_modal').modal('show');
                } else if (status == 'Inactive') {
                    $('#is_inactall_modal').modal('show');
                } else {
                    $('#is_dltall_modal').modal('show');
                }

                $(".action_modal_submit").unbind().click(function () {
                    var action = $("#pageForm").attr('action');
                    var formValus = $("#_list_form, #pageForm").serialize();
                    window.location.href = action + "?" + formValus;
                });
                $("#changeStatus").val('');
            } else {
                $('#is_not_check_modal').modal('show');
                $("#changeStatus").val('');
                return false;
            }
        } else {
            return false;
        }
    }


    function changeStatus(iAdminId, status) {
//		$('html').addClass('loading');
        var action = $("#pageForm").attr('action');
        var page = $("#page").val();
        if (status == 'Active') {
            status = 'Inactive';
        } else {
            status = 'Active';
        }
        $("#page").val(page);
        $("#iMainId01").val(iAdminId);
        $("#status01").val(status);
        var formValus = $("#pageForm").serialize();
        window.location.href = action + "?" + formValus;
    }
    //make
    /* function changeStatus(iMakeId,status) {
     //		$('html').addClass('loading');
     var action = $("#pageForm").attr('action');
     var page = $("#page").val();
     if(status == 'Active') {
     status = 'Inactive';
     }else {
     status = 'Active';
     }
     $("#page").val(page);
     $("#iMainId01").val(iAdminId);
     $("#status01").val(status);
     var formValus = $("#pageForm").serialize();
     window.location.href = action+"?"+formValus;
     } */
    //make
    function changeStatusDelete(iAdminId) {

        $('#is_dltSngl_modal').modal('show');

        $(".action_modal_submit").unbind().click(function () {
//			$('html').addClass('loading');

            var action = $("#pageForm").attr('action');

            var page = $("#pageId").val();
            $("#pageId01").val(page);
            $("#iMainId01").val(iAdminId);
            $("#method").val('delete');
            var formValus = $("#pageForm").serialize();
            window.location.href = action + "?" + formValus;

        });
    }
    function changeStatusDeletevehicle(iAdminId, driverid) {
        $('#is_dltSngl_modal').modal('show');
        $(".action_modal_submit").unbind().click(function () {
//			$('html').addClass('loading');
            var action = $("#pageForm").attr('action');
            var page = $("#pageId").val();
            $("#pageId01").val(page);
            $("#iMainId01").val(iAdminId);
            $("#iDriverId").val(driverid);
            $("#method").val('delete');
            var formValus = $("#pageForm").serialize();
            window.location.href = action + "?" + formValus;

        });
    }

    function changeStatusDeletecd(iAdminId) {
        $('#is_dltSngl_modal_cd').modal('show');
        $(".action_modal_submit").unbind().click(function () {
//			$('html').addClass('loading');
            var action = $("#pageForm").attr('action');
            var page = $("#pageId").val();
            $("#pageId01").val(page);
            $("#iMainId01").val(iAdminId);
            $("#method").val('delete');
            var formValus = $("#pageForm").serialize();
            window.location.href = action + "?" + formValus;

        });
    }

    function changeStatusDeletestore(iAdminId) {
        $('#is_dltSngl_modal_store').modal('show');
        $(".action_modal_submit").unbind().click(function () {
//			$('html').addClass('loading');
            var action = $("#pageForm").attr('action');
            var page = $("#pageId").val();
            $("#pageId01").val(page);
            $("#iMainId01").val(iAdminId);
            $("#method").val('delete');
            var formValus = $("#pageForm").serialize();
            window.location.href = action + "?" + formValus;

        });
    }


    function resetTripStatus(iAdminId) {
        $('#is_resetTrip_modal').modal('show');
        $(".action_modal_submit").unbind().click(function () {
//			$('html').addClass('loading');
            var action = $("#pageForm").attr('action');
            var page = $("#pageId").val();
            $("#pageId01").val(page);
            $("#iMainId01").val(iAdminId);
            $("#method").val('reset');
            var formValus = $("#pageForm").serialize();
            window.location.href = action + "?" + formValus;
        });
    }

    function showExportTypes(section) {
        if (section == 'store_review') {
            $("#show_export_types_modal_excel").modal('show');
            $("#export_modal_submit_excel").on('click', function () {
                var action = "main_export.php";
                var formValus = $("#_export_form, #pageForm, #show_export_modal_form_excel").serialize();
                //alert(formValus);
                window.location.href = action + '?section=' + section + '&' + formValus;
                $("#show_export_types_modal_excel").modal('hide');
                return false;
            });
        } else {
            $("#show_export_types_modal").modal('show');
            $("#export_modal_submit").on('click', function () {
                var action = "main_export.php";
                var formValus = $("#_export_form, #pageForm, #show_export_modal_form").serialize();
                window.location.href = action + '?section=' + section + '&' + formValus;
                $("#show_export_types_modal").modal('hide');
                return false;
            });
        }
    }
    function Redirect(sortby, order) {
        //$('html').addClass('loading');
        $("#sortby").val(sortby);
        if (order == 0) {
            order = 1;
        } else {
            order = 0;
        }
        $("#order").val(order);
        $("#page").val('1');
        var action = $("#_list_form").attr('action');
        var formValus = $("#pageForm").serialize();
        //alert(formValus);
        window.location.href = action + "?" + formValus;
    }

    function reset_form(formId) {
        $("#" + formId).find("input[type=text],input[type=password],input[type=file], textarea, select").val("");
    }

    //function openHoverAction(openId) {
    $('.openHoverAction-class').click(function (e) {
        // openHoverAction-class
        //e.preventDefault();
        alert('hiii');
        // hide all span
        var $this = $(this).find('.show-moreOptions');
        $(".openHoverAction-class .show-moreOptions").not($this).hide();

        // here is what I want to do
        $this.toggle();

//            if($(".openPops_"+openId).hasClass('active')) {
//                $('.show-moreOptions').removeClass('active');
//            }else {
//                
//            }
//            $(".openPops_"+openId).addClass('active');
    });
    function reportExportTypes(section) {
        var action = "report_export.php";
        var formValus = $("#pageForm").serialize();
        //alert(formValus);
        window.location.href = action + '?section=' + section + '&' + formValus;
        return false;
    }

    function Paytodriver() {
        var checked = $("#_list_form input:checked").length;
        if (checked > 0) {
            if (checked == 1) {
                if ($("#_list_form input:checked").attr("id") == 'setAllCheck') {
                    $('#is_not_check_modal').modal('show');
                    $("#changeStatus").val('');
                    return false;
                }
            }
            $('#is_payTo_modal').modal('show');
            $(".action_modal_submit").unbind().click(function () {
                $("#ePayDriver").val('Yes');
                var action = $("#pageForm").attr('action');
                var formValus = $("#_list_form, #pageForm").serialize();
                window.location.href = action + "?" + formValus;
            });
        } else {
            $('#is_not_check_modal').modal('show');
            return false;
        }
    }

    function PaytodriverforCancel() {
        var checked = $("#_list_form input:checked").length;
        if (checked > 0) {
            if (checked == 1) {
                if ($("#_list_form input:checked").attr("id") == 'setAllCheck') {
                    $('#is_not_check_modal').modal('show');
                    $("#changeStatus").val('');
                    return false;
                }
            }
            $('#is_payTo_modal').modal('show');
            $(".action_modal_submit").unbind().click(function () {
                $("#ePayDriver").val('Yes');
                var action = $("#pageForm").attr('action');
                var formValus = $("#_list_form, #pageForm").serialize();
                window.location.href = action + "?" + formValus;
            });
        } else {
            $('#is_not_check_modal').modal('show');
            return false;
        }
    }
    function PaytoorganizationforCancel() {
        var checked = $("#_list_form input:checked").length;
        if (checked > 0) {
            if (checked == 1) {
                if ($("#_list_form input:checked").attr("id") == 'setAllCheck') {
                    $('#is_not_check_modal').modal('show');
                    $("#changeStatus").val('');
                    return false;
                }
            }
            $('#is_payTo_organization_modal').modal('show');
            $(".action_modal_submit").unbind().click(function () {
                $("#ePayDriver").val('Yes');
                var action = $("#pageForm").attr('action');
                var formValus = $("#_list_form, #pageForm").serialize();
                window.location.href = action + "?" + formValus;
            });
        } else {
            $('#is_not_check_modal').modal('show');
            return false;
        }
    }

    function changeOrder(iAdminId) {
        $('#is_dltSngl_modal').modal('show');
        $(".action_modal_submit").unbind().click(function () {
            var action = $("#pageForm").attr('action');
            var page = $("#pageId").val();
            $("#pageId01").val(page);
            $("#iMainId01").val(iAdminId);
            $("#method").val('delete');
            var formValus = $("#pageForm").serialize();
            window.location.href = action + "?" + formValus;
        });
    }

    function PaytoRestaurant() {

        var checked = $("#_list_form input:checked").length;

        if (checked > 0) {
            if (checked == 1) {

                if ($("#_list_form input:checked").attr("id") == 'setAllCheck') {
                    $('#is_not_check_modal').modal('show');
                    $("#changeStatus").val('');
                    return false;
                }
            }
            $('#is_payTo_Res_modal').modal('show');
            $(".action_modal_submit").unbind().click(function () {
                $("#ePayRestaurant").val('Yes');
                var action = $("#pageForm").attr('action');
                var formValus = $("#_list_form, #pageForm").serialize();
                window.location.href = action + "?" + formValus;
            });
        } else {
            $('#is_not_check_modal').modal('show');
            return false;
        }
    }

</script>

<!-- Select Error -->
<div data-backdrop="static" data-keyboard="false" class="modal fade" id="is_not_check_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4></h4></div>
            <div class="modal-body"><p>Please Check At Least One Record</p></div>
            <div class="modal-footer"><button type="button" class="btn btn-danger btn-ok" data-dismiss="modal">OK</button></div>
        </div>
    </div>
</div>
<!-- Active Modal -->
<div data-backdrop="static" data-keyboard="false" class="modal fade" id="is_actall_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4>Active Record(s) ?</h4></div>
            <div class="modal-body"><p id="new-msg-activeid">Are you sure you want to activate selected record(s)?</p></div>
            <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Not Now</button><a class="btn btn-success btn-ok action_modal_submit" >Yes</a></div>
        </div>
    </div>
</div>
<!-- Inactive Modal -->
<div data-backdrop="static" data-keyboard="false" class="modal fade" id="is_inactall_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4>Inactive Record(s) ?</h4></div>
            <div class="modal-body"><p>Are you sure you want to inactivate selected record(s)?</p></div>
            <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Not Now</button><a class="btn btn-success btn-ok action_modal_submit" >Yes</a></div>
        </div>
    </div>
</div>
<!-- DeleteAll Modal -->
<div data-backdrop="static" data-keyboard="false" class="modal fade" id="is_dltall_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4>Delete Record(s) ?</h4></div>
            <div class="modal-body"><p>Are you sure you want to delete selected records?</p></div>
            <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Not Now</button><a class="btn btn-success btn-ok action_modal_submit" >Yes</a></div>
        </div>
    </div>
</div>

<!-- Delete Single Modal -->
<div data-backdrop="static" data-keyboard="false" class="modal fade" id="is_dltSngl_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4>Delete Record(s) ?</h4></div>
            <div class="modal-body"><p>Are you sure you want to delete this record?</p></div>
            <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Not Now</button><a class="btn btn-success btn-ok action_modal_submit" >Yes</a></div>
        </div>
    </div>
</div>


<div data-backdrop="static" data-keyboard="false" class="modal fade" id="is_dltSngl_modal_cd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4>Delete Record(s) ?</h4></div>
            <div class="modal-body"><p> This company contains providers under it. If you delete company then all providers will be assigned to default company. Confirm to delete company?</p></div>
            <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Not Now</button><a class="btn btn-success btn-ok action_modal_submit" >Yes</a></div>
        </div>
    </div>
</div>
<div data-backdrop="static" data-keyboard="false" class="modal fade" id="is_dltSngl_modal_store" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4>Delete Record(s) ?</h4></div>
            <div class="modal-body"><p> Are you sure you want to delete the ?<br>
            Note : Kindly delete the item category and Items if exists once you delete the .</p></div>
            <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Not Now</button><a class="btn btn-success btn-ok action_modal_submit" >Yes</a></div>
        </div>
    </div>
</div>
<!-- Reset Trip Status Modal -->
<div data-backdrop="static" data-keyboard="false" class="modal fade" id="is_resetTrip_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4>Reset Record(s) ?</h4></div>
            <div class="modal-body"><p>Are you sure? You want to reset selected account?</p></div>
            <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Not Now</button><a class="btn btn-success btn-ok action_modal_submit">Yes</a></div>
        </div>
    </div>
</div>

<!-- Export Modal -->
<div class="modal fade" id="show_export_types_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4>Export to file : </h4></div>
            <div class="modal-body">
                <form name="show_export_modal_form" id="show_export_modal_form" action="" method="post" class="form-horizontal form-box remove-margin" enctype="multipart/form-data">
                    <div class="form-box-content export-popup">
                        <b>Select File type : </b>
                        <span><input type="radio" name="exportType" value="XLS" checked>Excel/CSV</span>
                        <!--<span><input type="radio" name="exportType" value="PDF">PDF</span>-->
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <p style="text-align: left;">Please <a href="excelfilesteps.html" target="_blank">click here</a> to follow the steps, If your expoerted CSV files shows special characters corrupted.</a></p> <br/>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button><a id="export_modal_submit" class="btn btn-primary">Download</a>
            </div>
        </div>
    </div>
</div>
 
<!-- Export Excel Modal --><!-- For blocked To Driver/Rider Modal -->
<div class="modal fade" id="show_export_types_modal_excel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4>Export to file : </h4></div>
            <div class="modal-body">
                <form name="show_export_modal_form_excel" id="show_export_modal_form_excel" action="" method="post" class="form-horizontal form-box remove-margin" enctype="multipart/form-data">
                    <div class="form-box-content export-popup">
                        <b>Select File type : </b>
                        <span><input type="radio" name="exportType" value="XLS" checked>Excel/CSV</span>
                        <!-- <span><input type="radio" name="exportType" value="PDF">PDF</span> -->
                    </div>
                </form>
            </div>
            <div class="modal-footer"><p style="text-align: left;">Please <a href="excelfilesteps.html" target="_blank">click here</a> and follow the steps, if the exported CVS file is corrupted or shows junk characters.</a></p> <br/>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button><a id="export_modal_submit_excel" class="btn btn-primary">Download</a></div>
        </div>
    </div>
</div>

<!-- Pay To Driver Modal -->
<div data-backdrop="static" data-keyboard="false" class="modal fade" id="is_payTo_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4>Pay to Provider (s)?</h4></div>
            <div class="modal-body"><p>Are you sure you want to Pay To Provider (s)?</p></div>
            <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Not Now</button><a class="btn btn-success btn-ok action_modal_submit" >Yes</a></div>
        </div>
    </div>
</div>

<!-- Pay To Restaurant Modal -->

<!-- Pay To Driver Modal -->
<div data-backdrop="static" data-keyboard="false" class="modal fade" id="is_payTo_organization_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4>Pay by Organization(s) ?</h4></div>
            <div class="modal-body"><p>Are you sure you have collected amount from organization(s) ?</p></div>
            <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Not Now</button><a class="btn btn-success btn-ok action_modal_submit" >Yes</a></div>
        </div>
    </div>
</div>

<!-- Pay To Restaurant Modal -->


<div data-backdrop="static" data-keyboard="false" class="modal fade" id="is_payTo_Res_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4>Pay to Restaurant(s) ?</h4></div>
            <div class="modal-body"><p>Are you sure you want to Pay To Restaurant(s)?</p></div>
            <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Not Now</button><a class="btn btn-success btn-ok action_modal_submit" >Yes</a></div>
        </div>
    </div>
</div>        <link href="../assets/css/jquery-ui.css" rel="stylesheet" />
        <link rel="stylesheet" href="../assets/plugins/switch/static/stylesheets/bootstrap-switch.css" />
        <link rel="stylesheet" href="../assets/validation/validatrix.css" />
    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >
        <!-- MAIN WRAPPER -->
        <div id="wrap">
            <!-- HEADER SECTION -->
<script>
    var _system_admin_url = 'https://finelooks.co.uk/admin/';
</script>
<script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<!-- <script src="js/New/perfect-scrollbar.js"></script> -->
<!-- END GLOBAL SCRIPTS -->
<!-- END HEADER SECTION -->
<link type="text/css" href="css/admin_new/admin_style.css" rel="stylesheet" />
<!--<link type="text/css" href="css/adminLTE/AdminLTE.min.css" rel="stylesheet" />-->
<input type="hidden" name="baseurl" id="baseurl" value="">
<div class="wrapper1">
    <div class="new-mobile001">
        <nav class="navbar navbar-inverse navbar-fixed-top" style="padding:7px 0;">
            <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#sidebar" id="menu-toggle"><i class="icon-align-justify"></i></a>
        </nav>
    </div>
    <header class="main_header">
        <div class="header clearfix">
            <a href="dashboard.php" title="" class="logo"> <span class="logo-mini"> <img src="images/logo-small.png" alt="" /> </span> <span class="logo-lg minus"> <img src="images/admin-logo.png" alt="" /> </span> </a>
            <nav class="navbar-static-top"> 
                <a class="sidebar-toggle" href="javascript:void(0);" data-toggle="tooltip" data-placement="right" title="show / hide sidebar"></a>
                <span style="margin: 26px 0 0 20px;float: left;">Abubakar&nbsp;&nbsp;Abrar</span>
            </nav>
            <div>
                <a href="logout.php" title="Logout" class="header-top-button"><img src="images/logout-icon1.png" alt="" />Logout</a>
                <!-- <div id="google_translate_element" class="header-top-translate-button"></div> -->
            </div>
        </div>
    </header>
    <div class="main-sidebar">
<section class="sidebar">
    <!-- Sidebar -->
    <div id="sidebar" class="test" >
        <nav class="menu">
            <ul class='sidebar-menu' style=''><li  class=''><a href='dashboard.php' ><i class='fa fa-tachometer' aria-hidden='true'></i><span>Dashboard</span></a></li><li  class=''><a href='dashboard-a.php' ><i class='fa fa-sitemap' aria-hidden='true'></i><span>Site Statistics</span></a></li><li  class='treeview'><a href='javascript:' class='expand' ><i class='icon-user1' aria-hidden='true'><img src='images/icon/admin-icon.png' /></i><span>Admin</span></a><ul class='treeview-menu menu_drop_down' style='display:none'><li  class=''><a href='admin.php' ><i class='fa fa-certificate' aria-hidden='true'></i>Admin</a></li><li  class=''><a href='admin_groups.php' ><i class='fa fa-certificate' aria-hidden='true'></i>Admin Groups</a></li></ul></li><li  class=''><a href='company.php' ><i class='fa fa-building-o' aria-hidden='true'></i><span>Company</span></a></li><li  class='active'><a href='driver.php' ><i class='icon-user1' aria-hidden='true'><img src='images/icon/driver-icon.png' /></i><span>Provider</span></a></li><li  class=''><a href='rider.php' ><i class='icon-group1' aria-hidden='true'><img src='images/rider-icon.png' /></i><span>User</span></a></li><li  class='treeview'><a href='javascript:' class='expand' ><i class='fa fa-wrench' aria-hidden='true'></i><span>Manage Services</span></a><ul class='treeview-menu menu_drop_down' style='display:none'><li  class=''><a href='vehicle_category.php' ><i class='fa fa-certificate' aria-hidden='true'></i>Service Category</a></li><li  class=''><a href='service_type.php' ><i class='fa fa-wrench' aria-hidden='true'></i>Service Type</a></li><li  class=''><a href='driver_service_request.php' ><i class='fa fa-wrench' aria-hidden='true'></i>Provider Service Requests</a></li></ul></li><li  class=''><a href='add_booking.php' ><i class='fa fa-taxi1' aria-hidden='true'><img src='images/manual-taxi-icon.png' /></i><span>Manual Booking</span></a></li><li  class=''><a href='trip.php' ><i class='fa fa-exchange1' aria-hidden='true'><img src='images/trips-icon.png' /></i><span>Jobs</span></a></li><li  class=''><a href='cab_booking.php' ><i class='icon-book1' aria-hidden='true'><img src='images/ride-later-bookings.png' /></i><span>Scheduled Bookings</span></a></li><li  class=''><a href='coupon.php' ><i class='fa fa-product-hunt1' aria-hidden='true'><img src='images/promo-code-icon.png' /></i><span>PromoCode</span></a></li><li  class=''><a href='map.php' ><i class='icon-map-marker1' aria-hidden='true'><img src='images/god-view-icon.png' /></i><span>God's View</span></a></li><li  class=''><a href='heatmap.php' ><i class='fa-header1' aria-hidden='true'><img src='images/heat-icon.png' /></i><span>Heat View</span></a></li><li  class=''><a href='review.php' ><i class='icon-comments1' aria-hidden='true'><img src='images/reviews-icon.png' /></i><span>Reviews</span></a></li><li  class=''><a href='advertise_banners.php' ><i class='fa fa-bullhorn' aria-hidden='true'></i><span>Advertisement Banners</span></a></li><li  class='treeview active'><a href='javascript:' class='expand' ><i class='fa fa-bullhorn' aria-hidden='true'></i><span>Decline/Cancelled Alert</span></a><ul class='treeview-menu menu_drop_down' style='display:none'><li  class=''><a href='blocked_driver.php' ><i class='fa fa-user' aria-hidden='true'></i>Alert For Provider</a></li><li  class=''><a href='blocked_rider.php' ><i class='fa fa-user' aria-hidden='true'></i>Alert For User</a></li></ul></li><li  class='treeview'><a href='javascript:' class='expand' ><i class='icon-cogs1' aria-hidden='true'><img src='images/reports-icon.png' /></i><span>Reports</span></a><ul class='treeview-menu menu_drop_down' style='display:none'><li  class=''><a href='payment_report.php' ><i class='icon-money' aria-hidden='true'></i>Payment Report</a></li><li  class=''><a href='wallet_report.php' ><i class='fa fa-google-wallet' aria-hidden='true'></i>Wallet Report</a></li><li  class=''><a href='driver_pay_report.php' ><i class='icon-money' aria-hidden='true'></i>Provider Payment Report</a></li></ul></li><li  class='treeview'><a href='javascript:' class='expand' ><i class='fa fa-header1' aria-hidden='true'><img src='images/location-icon.png' /></i><span>Manage Locations</span></a><ul class='treeview-menu menu_drop_down' style='display:none'><li  class=''><a href='location.php' ><i class='fa fa-map-marker' aria-hidden='true'></i>Geo Fence Location</a></li><li  class=''><a href='restricted_area.php' ><i class='fa fa-map-signs' aria-hidden='true'></i>Restricted Area</a></li></ul></li><li  class='treeview'><a href='javascript:' class='expand' ><i class='icon-cogs1' aria-hidden='true'><img src='images/settings-icon.png' /></i><span>Settings</span></a><ul class='treeview-menu menu_drop_down' style='display:none'><li  class=''><a href='general.php' ><i class='fa-cogs fa' aria-hidden='true'></i>General</a></li><li  class=''><a href='email_template.php' ><i class='fa fa-envelope' aria-hidden='true'></i>Email Templates</a></li><li  class=''><a href='sms_template.php' ><i class='fa fa-comment' aria-hidden='true'></i>SMS Templates</a></li><li  class=''><a href='document_master_list.php' ><i class='fa fa-file-text' aria-hidden='true'></i>Manage Documents</a></li><li  class=''><a href='languages.php' ><i class='fa fa-language' aria-hidden='true'></i>Language Label</a></li><li  class=''><a href='currency.php' ><i class='fa fa-usd' aria-hidden='true'></i>Currency</a></li><li  class=''><a href='language.php' ><i class='fa fa-language' aria-hidden='true'></i>Language</a></li><li  class=''><a href='seo_setting.php' ><i class='fa fa-info' aria-hidden='true'></i>SEO Settings</a></li><li  class=''><a href='banner.php' ><i class='icon-angle-right' aria-hidden='true'></i>Banner</a></li></ul></li><li  class='treeview'><a href='javascript:' class='expand' ><i class='fa fa-wrench' aria-hidden='true'></i><span>Utility</span></a><ul class='treeview-menu menu_drop_down' style='display:none'><li  class='treeview'><a href='javascript:' class='expand' ><i class='fa fa-globe' aria-hidden='true'></i>Localization</a><ul class='treeview-menu menu_drop_down' style='display:none'><li  class=''><a href='country.php' ><i class='fa fa-dot-circle-o' aria-hidden='true'></i>Country</a></li><li  class=''><a href='state.php' ><i class='fa fa-dot-circle-o' aria-hidden='true'></i>State</a></li></ul></li><li  class=''><a href='page.php' ><i class='fa fa-file-text-o' aria-hidden='true'></i>Pages</a></li><li  class=''><a href='home_content.php' ><i class='fa fa-file-text-o' aria-hidden='true'></i>Edit Home Page</a></li><li  class=''><a href='news.php' ><i class='fa fa-file-text-o' aria-hidden='true'></i>News</a></li><li  class=''><a href='newsletter.php' ><i class='fa fa-file-text-o' aria-hidden='true'></i>Newsletter Subscribers</a></li><li  class=''><a href='faq.php' ><i class='fa fa-question' aria-hidden='true'></i>Faq</a></li><li  class=''><a href='faq_categories.php' ><i class='fa fa-question-circle-o' aria-hidden='true'></i>Faq Categories</a></li><li  class=''><a href='help_detail.php' ><i class='fa fa-question' aria-hidden='true'></i>Help Topics</a></li><li  class=''><a href='help_detail_categories.php' ><i class='fa fa-question-circle-o' aria-hidden='true'></i>Help Topic Categories</a></li><li  class=''><a href='cancellation_reason.php' ><i class='fa fa-question' aria-hidden='true'></i>Cancel Reason</a></li><li  class=''><a href='home_driver.php' ><i class='fa fa-users' aria-hidden='true'></i>Our Provider</a></li><li  class=''><a href='send_notifications.php' ><i class='fa fa-globe' aria-hidden='true'></i>Send Push-Notification</a></li><li  class=''><a href='backup.php' ><i class='fa fa-database' aria-hidden='true'></i>DB Backup</a></li><li  class=''><a href='system_diagnostic.php' ><i class='fa fa-sitemap' aria-hidden='true'></i>System Diagnostic</a></li></ul></li><li  class=''><a href='logout.php' ><i class='icon-signin1' aria-hidden='true'><img src='images/logout-icon.png' /></i><span>Logout</span></a></li></ul>        </nav>
    </div>
</section>    </div>
    <div class="loader-default"></div>
    <script>
    function setMenuEnable(id)
    {
        $.ajax({
            method: "post",
            url: _system_admin_url + "setMenuEnable.php",
            data: "data=" + id,
            cache: false,
            dataType: 'html',
            success: function (response) {
            }
        });
    }
    $(document).ready(function () {
        $.sidebarMenu($('.sidebar-menu'));
            $("body").removeClass("sidebar_hide");
            $("body").removeClass("sidebar-minize");
            $("body").removeClass("sidebar-collapse");
    });
    $.sidebarMenu = function (menu) {
        var animationSpeed = 300;
        $(menu).on('click', 'li a', function (e) {
            var $this = $(this);
            var checkElement = $this.next();
            if (checkElement.is('.treeview-menu') && checkElement.is(':visible')) {
                checkElement.slideUp(animationSpeed, function () {
                    checkElement.removeClass('menu-open');
                });
                checkElement.parent("li").removeClass("active");
            }
            //If the menu is not visible
            else if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible'))) {
                //Get the parent menu
                var parent = $this.parents('ul').first();
                //Close all open menus within the parent
                var ul = parent.find('ul:visible').slideUp(animationSpeed);
                //Remove the menu-open class from the parent
                ul.removeClass('menu-open');
                //Get the parent li
                var parent_li = $this.parent("li");
                //Open the target menu and add the menu-open class
                checkElement.slideDown(animationSpeed, function () {
                    //Add the class active to the parent li
                    checkElement.addClass('menu-open');
                    parent.find('li.active').removeClass('active');
                    parent_li.addClass('active');
                });
            }
            //if this isn't a link, prevent the page from being redirected
            if (checkElement.is('.treeview-menu')) {
                e.preventDefault();
            }
        });
    }
    </script>
    <!-- /footer -->
</div>
<!-- END HEADER SECTION -->
<script type="text/javascript">
    $(document).ready(function () {
        if ($('#messagedisplay')) {
            $('#messagedisplay').animate({opacity: 1.0}, 2000)
            $('#messagedisplay').fadeOut('slow');
        }
        //for side bar menu
        $(".content-wrapper").css({'min-height': ($(".wrapper .main-sidebar").height() + 'px')});
        $('.sidebar-toggle').click(function () {
            $("body").toggleClass("sidebar_hide");
            if ($("body").hasClass("sidebar_hide")) {
                $("body").addClass("sidebar-minize");
                $("body").addClass("sidebar-collapse");
                setMenuEnable(0);
            } else {
                $("body").removeClass("sidebar-minize");
                $("body").removeClass("sidebar-collapse");
                setMenuEnable(1);
            }
        });
        $("#content").addClass('content_right');
        if ($(window).width() < 800) {
            $('.sidebar-toggle').click(function () {
                $("body").toggleClass("sidebar_hide");
                if ($("body").hasClass("sidebar_hide")) {
                    $("body").addClass("sidebar-open");
                    $("body").removeClass("sidebar-collapse");
                    setMenuEnable(0);
                } else {
                    $("body").removeClass("sidebar-open");
                    $("body").removeClass("sidebar-collapse");
                    setMenuEnable(1);
                }
            });
        }
        if ($(window).width() < 900) {
            $("body").removeClass("sidebar-collapse");
            $('.sidebar-toggle').click(function () {
                $('body').toggleClass('sidebar-open');
                if (sessionStorage.sidebarin == 0) {
                    $("body").addClass("sidebar-minize");
                    $("body").removeClass("sidebar-collapse");
                } else {
                    $("body").removeClass("sidebar-minize");
                    $("body").removeClass("sidebar-collapse");
                }
            });
        }
    });
</script>
<script type="text/javascript">
//===== Hide/show Menubar =====//
    $('.fullview').click(function () {
        $("body").toggleClass("clean");
        $('#sidebar').toggleClass("show-sidebar mobile-sidebar");
        $('#content').toggleClass("full-content");
    });
    $(window).resize(function () {
        if ($(window).width() < 900) {
            if (sessionStorage.sidebarin == 0) {
                $("body").addClass("sidebar-minize");
                $("body").removeClass("sidebar-collapse");
            } else {
                $("body").removeClass("sidebar-minize");
                $("body").removeClass("sidebar-collapse");
            }
        }
    });
</script>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
    $(window).load(function () {
        $(".loader-default").fadeOut("slow");
    });
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script type="text/javascript">
    function googleTranslateElementInit() {
        //new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
    }
</script>

                        <!--PAGE CONTENT -->
            <div id="content">
                <div class="inner">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Edit Service</h2>
                            <a href="driver.php" class="back_link">
                                <input type="button" value="Back To Listing" class="add-btn">
                            </a>
                        </div>
                    </div>
                    <hr />
                    <div class="body-div">
                        <div class="form-group">
                                                                                        <form name="vehicle_form" id="vehicle_form" method="post" action="">	
                                    <input type="hidden" name="iDriverId"  value="2001"/>
                                    <input type="hidden" name="iCompanyId"  value="1"/>
                                    <input type="hidden" name="iMakeId"  value="3"/>
                                    <input type="hidden" name="iModelId"  value="1"/>
                                    <input type="hidden" name="iYear"  value="2024"/>
                                    <input type="hidden" name="vLicencePlate"  value="My Services"/>
                                    <input type="hidden" name="id" value="2007"/>
                                    <input type="hidden" name="eType"  value="UberX"/>
                                    <input type="hidden" name="previousLink" id="previousLink" value=""/>
                                    <input type="hidden" name="backlink" id="backlink" value="driver.php"/>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <label>Service Type <span class="red">*</span></label>
                                        </div>
                                    </div>
                                    <div class="checkbox-group required add-services-hatch car-type-custom">
                                        <ul>
                                                                                                    <fieldset>
                                                            
                                                            <legend>
                                                                <strong>Beauty Services - Mens Hair</strong>
                                                            </legend>

                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Standard Haircut & Styling (express)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_63" onchange="check_box_value(this.value);"   value="63"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_63" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_63" value="Men - Standard Haircut & Styling (express)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[63]" value="1500" placeholder="Enter Amount for Men - Standard Haircut & Styling (express)" id="fAmount_63" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Shave & Styling<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_64" onchange="check_box_value(this.value);"   value="64"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_64" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_64" value="Men - Shave & Styling">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[64]" value="1500" placeholder="Enter Amount for Men - Shave & Styling" id="fAmount_64" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Lowfade<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_65" onchange="check_box_value(this.value);"   value="65"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_65" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_65" value="Men - Lowfade">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[65]" value="500" placeholder="Enter Amount for Men - Lowfade" id="fAmount_65" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Haircut & Beard<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_66" onchange="check_box_value(this.value);"   value="66"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_66" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_66" value="Men - Haircut & Beard">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[66]" value="2500" placeholder="Enter Amount for Men - Haircut & Beard" id="fAmount_66" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Haircut & Wash<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_396" onchange="check_box_value(this.value);"   value="396"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_396" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_396" value="Men - Haircut & Wash">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[396]" value="1500" placeholder="Enter Amount for Men - Haircut & Wash" id="fAmount_396" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Zero all over<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_397" onchange="check_box_value(this.value);"   value="397"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_397" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_397" value="Men - Zero all over">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[397]" value="1000" placeholder="Enter Amount for Men - Zero all over" id="fAmount_397" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Kids - Basic Cut Under 12s<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_398" onchange="check_box_value(this.value);"   value="398"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_398" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_398" value="Kids - Basic Cut Under 12s">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[398]" value="500" placeholder="Enter Amount for Kids - Basic Cut Under 12s" id="fAmount_398" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Kid - Skinfade for under 12s<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_399" onchange="check_box_value(this.value);"   value="399"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_399" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_399" value="Kid - Skinfade for under 12s">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[399]" value="500" placeholder="Enter Amount for Kid - Skinfade for under 12s" id="fAmount_399" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Colour, Cut & Finish<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_400" onchange="check_box_value(this.value);"   value="400"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_400" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_400" value="Men - Colour, Cut & Finish">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[400]" value="3000" placeholder="Enter Amount for Men - Colour, Cut & Finish" id="fAmount_400" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Wet Shave<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_401" onchange="check_box_value(this.value);"   value="401"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_401" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_401" value="Men - Wet Shave">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[401]" value="1000" placeholder="Enter Amount for Men - Wet Shave" id="fAmount_401" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Basic Haircut<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_482" onchange="check_box_value(this.value);"   value="482"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_482" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_482" value="Men - Basic Haircut">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[482]" value="1000" placeholder="Enter Amount for Men - Basic Haircut" id="fAmount_482" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Skin Fade & Beard Trim<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_576" onchange="check_box_value(this.value);"   value="576"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_576" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_576" value="Men - Skin Fade & Beard Trim">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[576]" value="1500" placeholder="Enter Amount for Men - Skin Fade & Beard Trim" id="fAmount_576" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Skin Fade<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_475" onchange="check_box_value(this.value);"   value="475"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_475" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_475" value="Men - Skin Fade">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[475]" value="700" placeholder="Enter Amount for Men - Skin Fade" id="fAmount_475" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Hair Wash<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_476" onchange="check_box_value(this.value);"   value="476"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_476" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_476" value="Men - Hair Wash">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[476]" value="250" placeholder="Enter Amount for Men - Hair Wash" id="fAmount_476" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Wash, Haircut & Blow Dry<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_483" onchange="check_box_value(this.value);"   value="483"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_483" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_483" value="Men - Wash, Haircut & Blow Dry">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[483]" value="2000" placeholder="Enter Amount for Men - Wash, Haircut & Blow Dry" id="fAmount_483" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Wash, Haircut & Styling<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_484" onchange="check_box_value(this.value);"   value="484"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_484" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_484" value="Men - Wash, Haircut & Styling">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[484]" value="2500" placeholder="Enter Amount for Men - Wash, Haircut & Styling" id="fAmount_484" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Wash, Haircut & Shave<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_485" onchange="check_box_value(this.value);"   value="485"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_485" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_485" value="Men - Wash, Haircut & Shave">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[485]" value="2000" placeholder="Enter Amount for Men - Wash, Haircut & Shave" id="fAmount_485" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Clipper Haircut (one length all over)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_487" onchange="check_box_value(this.value);"   value="487"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_487" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_487" value="Men - Clipper Haircut (one length all over)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[487]" value="1000" placeholder="Enter Amount for Men - Clipper Haircut (one length all over)" id="fAmount_487" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Scissor Haircut<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_488" onchange="check_box_value(this.value);"   value="488"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_488" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_488" value="Men - Scissor Haircut">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[488]" value="1000" placeholder="Enter Amount for Men - Scissor Haircut" id="fAmount_488" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Machine Haircut (1 Size all Over)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_489" onchange="check_box_value(this.value);"   value="489"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_489" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_489" value="Men - Machine Haircut (1 Size all Over)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[489]" value="1000" placeholder="Enter Amount for Men - Machine Haircut (1 Size all Over)" id="fAmount_489" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Shave or Beard Design<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_490" onchange="check_box_value(this.value);"   value="490"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_490" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_490" value="Men - Shave or Beard Design">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[490]" value="1000" placeholder="Enter Amount for Men - Shave or Beard Design" id="fAmount_490" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Head Shave<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_491" onchange="check_box_value(this.value);"   value="491"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_491" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_491" value="Men - Head Shave">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[491]" value="15" placeholder="Enter Amount for Men - Head Shave" id="fAmount_491" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Haircut<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_509" onchange="check_box_value(this.value);"   value="509"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_509" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_509" value="Men - Haircut">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[509]" value="1000" placeholder="Enter Amount for Men - Haircut" id="fAmount_509" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Hot Towel Shave<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_574" onchange="check_box_value(this.value);"   value="574"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_574" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_574" value="Men - Hot Towel Shave">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[574]" value="1000" placeholder="Enter Amount for Men - Hot Towel Shave" id="fAmount_574" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Haircut Tattoo<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_575" onchange="check_box_value(this.value);"   value="575"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_575" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_575" value="Men - Haircut Tattoo">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[575]" value="2000" placeholder="Enter Amount for Men - Haircut Tattoo" id="fAmount_575" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Hair Dye<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_579" onchange="check_box_value(this.value);"   value="579"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_579" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_579" value="Men - Hair Dye">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[579]" value="2000" placeholder="Enter Amount for Men - Hair Dye" id="fAmount_579" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Shape Up<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_580" onchange="check_box_value(this.value);"   value="580"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_580" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_580" value="Men - Shape Up">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[580]" value="1000" placeholder="Enter Amount for Men - Shape Up" id="fAmount_580" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Kids - Shape Up<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_581" onchange="check_box_value(this.value);"   value="581"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_581" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_581" value="Kids - Shape Up">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[581]" value="500" placeholder="Enter Amount for Kids - Shape Up" id="fAmount_581" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Beard Trim<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_582" onchange="check_box_value(this.value);"   value="582"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_582" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_582" value="Men - Beard Trim">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[582]" value="800" placeholder="Enter Amount for Men - Beard Trim" id="fAmount_582" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - 0.5 Haircut<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_583" onchange="check_box_value(this.value);"  checked value="583"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_583" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_583" value="Men - 0.5 Haircut">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[583]" value="1000" placeholder="Enter Amount for Men - 0.5 Haircut" id="fAmount_583" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Kids - 0.5 Haircut<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_584" onchange="check_box_value(this.value);"  checked value="584"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_584" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_584" value="Kids - 0.5 Haircut">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[584]" value="500" placeholder="Enter Amount for Kids - 0.5 Haircut" id="fAmount_584" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Wax<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_585" onchange="check_box_value(this.value);"  checked value="585"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_585" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_585" value="Men - Wax">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[585]" value="500" placeholder="Enter Amount for Men - Wax" id="fAmount_585" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">

                                                                    <b>Kids - Scissor Cut<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_587" onchange="check_box_value(this.value);"  checked value="587"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_587" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_587" value="Kids - Scissor Cut">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[587]" value="500" placeholder="Enter Amount for Kids - Scissor Cut" id="fAmount_587" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Facial<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_588" onchange="check_box_value(this.value);"  checked value="588"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_588" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_588" value="Men - Facial">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[588]" value="2500" placeholder="Enter Amount for Men - Facial" id="fAmount_588" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Kids - Under 10s<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_599" onchange="check_box_value(this.value);"  checked value="599"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_599" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_599" value="Kids - Under 10s">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[599]" value="500" placeholder="Enter Amount for Kids - Under 10s" id="fAmount_599" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Threading<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_600" onchange="check_box_value(this.value);"  checked value="600"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_600" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_600" value="Men - Threading">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[600]" value="700" placeholder="Enter Amount for Men - Threading" id="fAmount_600" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Cleansing Face Scrub<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_601" onchange="check_box_value(this.value);"  checked value="601"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_601" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_601" value="Men - Cleansing Face Scrub">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[601]" value="500" placeholder="Enter Amount for Men - Cleansing Face Scrub" id="fAmount_601" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Cooling Face Mask<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_602" onchange="check_box_value(this.value);"  checked value="602"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_602" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_602" value="Men - Cooling Face Mask">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[602]" value="500" placeholder="Enter Amount for Men - Cooling Face Mask" id="fAmount_602" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Colour<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_603" onchange="check_box_value(this.value);"  checked value="603"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_603" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_603" value="Men - Colour">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[603]" value="1500" placeholder="Enter Amount for Men - Colour" id="fAmount_603" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Braids<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_674" onchange="check_box_value(this.value);"  checked value="674"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_674" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_674" value="Braids">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[674]" value="600" placeholder="Enter Amount for Braids" id="fAmount_674" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Hair Cut<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_675" onchange="check_box_value(this.value);"   value="675"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_675" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_675" value="Hair Cut">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[675]" value="15" placeholder="Enter Amount for Hair Cut" id="fAmount_675" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>shave wash and shoulder massager <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_688" onchange="check_box_value(this.value);"  checked value="688"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_688" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_688" value="shave wash and shoulder massager ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[688]" value="800" placeholder="Enter Amount for shave wash and shoulder massager " id="fAmount_688" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Locs retouch<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_694" onchange="check_box_value(this.value);"  checked value="694"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_694" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_694" value="Locs retouch">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[694]" value="1000" placeholder="Enter Amount for Locs retouch" id="fAmount_694" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Locs retouch<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_695" onchange="check_box_value(this.value);"  checked value="695"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_695" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_695" value="Locs retouch">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[695]" value="1000" placeholder="Enter Amount for Locs retouch" id="fAmount_695" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Locs re grafting <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_701" onchange="check_box_value(this.value);"  checked value="701"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_701" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_701" value="Locs re grafting ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[701]" value="6500" placeholder="Enter Amount for Locs re grafting " id="fAmount_701" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Locs styling<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_702" onchange="check_box_value(this.value);"  checked value="702"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_702" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_702" value="Locs styling">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[702]" value="500" placeholder="Enter Amount for Locs styling" id="fAmount_702" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Natural sisterlocs <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_703" onchange="check_box_value(this.value);"  checked value="703"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_703" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_703" value="Natural sisterlocs ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[703]" value="35000" placeholder="Enter Amount for Natural sisterlocs " id="fAmount_703" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Locs colouring <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_704" onchange="check_box_value(this.value);"  checked value="704"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_704" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_704" value="Locs colouring ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[704]" value="2500" placeholder="Enter Amount for Locs colouring " id="fAmount_704" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Locs detox<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_705" onchange="check_box_value(this.value);"  checked value="705"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_705" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_705" value="Locs detox">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[705]" value="2000" placeholder="Enter Amount for Locs detox" id="fAmount_705" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Locs shampooing <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_706" onchange="check_box_value(this.value);"  checked value="706"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_706" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_706" value="Locs shampooing ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[706]" value="1500" placeholder="Enter Amount for Locs shampooing " id="fAmount_706" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Retouch sister locs<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_719" onchange="check_box_value(this.value);"  checked value="719"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_719" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_719" value="Retouch sister locs">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[719]" value="2500" placeholder="Enter Amount for Retouch sister locs" id="fAmount_719" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>mens Hair cut<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_754" onchange="check_box_value(this.value);"  checked value="754"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_754" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_754" value="mens Hair cut">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[754]" value="300" placeholder="Enter Amount for mens Hair cut" id="fAmount_754" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Mens colour <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_755" onchange="check_box_value(this.value);"  checked value="755"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_755" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_755" value="Mens colour ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[755]" value="300" placeholder="Enter Amount for Mens colour " id="fAmount_755" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Kids_under 10yrs<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_756" onchange="check_box_value(this.value);"  checked value="756"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_756" style='display:block;'>
                                                                            <input type="hidden" name="desc" id="desc_756" value="Kids_under 10yrs">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[756]" value="150" placeholder="Enter Amount for Kids_under 10yrs" id="fAmount_756" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b><br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_764" onchange="check_box_value(this.value);"   value="764"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_764" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_764" value="">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[764]" value="0" placeholder="Enter Amount for " id="fAmount_764" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                    </fieldset>
                                                                                                                <fieldset>
                                                            
                                                            <legend>
                                                                <strong>Beauty Services - Womens Hair</strong>
                                                            </legend>

                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Colour<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_402" onchange="check_box_value(this.value);"   value="402"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_402" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_402" value="Ladies - Colour">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[402]" value="1000" placeholder="Enter Amount for Ladies - Colour" id="fAmount_402" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Wash and Restyle Haircut<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_498" onchange="check_box_value(this.value);"   value="498"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_498" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_498" value="Ladies - Wash and Restyle Haircut">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[498]" value="1000" placeholder="Enter Amount for Ladies - Wash and Restyle Haircut" id="fAmount_498" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Box Braids<br/>
                                                                        <span style="font-size: 12px;">(Location : London)</span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_409" onchange="check_box_value(this.value);"   value="409"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_409" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_409" value="Ladies - Box Braids">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[409]" value="1300" placeholder="Enter Amount for Ladies - Box Braids" id="fAmount_409" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Cornrows Removal<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_410" onchange="check_box_value(this.value);"   value="410"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_410" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_410" value="Ladies - Cornrows Removal">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[410]" value="150" placeholder="Enter Amount for Ladies - Cornrows Removal" id="fAmount_410" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Braids Removal<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_411" onchange="check_box_value(this.value);"   value="411"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_411" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_411" value="Ladies - Braids Removal">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[411]" value="200" placeholder="Enter Amount for Ladies - Braids Removal" id="fAmount_411" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies – Nubian twist<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_412" onchange="check_box_value(this.value);"   value="412"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_412" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_412" value="Ladies – Nubian twist">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[412]" value="2500" placeholder="Enter Amount for Ladies – Nubian twist" id="fAmount_412" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Kids – Braids<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_413" onchange="check_box_value(this.value);"   value="413"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_413" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_413" value="Kids – Braids">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[413]" value="600" placeholder="Enter Amount for Kids – Braids" id="fAmount_413" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Hair Roots Colouring<br/>
                                                                        <span style="font-size: 12px;">(Location : London)</span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_414" onchange="check_box_value(this.value);"   value="414"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_414" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_414" value="Ladies - Hair Roots Colouring">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[414]" value="20" placeholder="Enter Amount for Ladies - Hair Roots Colouring" id="fAmount_414" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Colouring Full Head<br/>
                                                                        <span style="font-size: 12px;">(Location : London)</span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_415" onchange="check_box_value(this.value);"   value="415"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_415" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_415" value="Ladies - Colouring Full Head">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[415]" value="35" placeholder="Enter Amount for Ladies - Colouring Full Head" id="fAmount_415" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Passion twist<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_416" onchange="check_box_value(this.value);"   value="416"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_416" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_416" value="Ladies - Passion twist">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[416]" value="2500" placeholder="Enter Amount for Ladies - Passion twist" id="fAmount_416" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Wash/Hydration Treatment/Blow dry<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_417" onchange="check_box_value(this.value);"   value="417"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_417" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_417" value="Ladies - Wash/Hydration Treatment/Blow dry">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[417]" value="1000" placeholder="Enter Amount for Ladies - Wash/Hydration Treatment/Blow dry" id="fAmount_417" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Extension locs<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_418" onchange="check_box_value(this.value);"   value="418"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_418" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_418" value="Ladies - Extension locs">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[418]" value="6700" placeholder="Enter Amount for Ladies - Extension locs" id="fAmount_418" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Single Plaits: Large boxes<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_429" onchange="check_box_value(this.value);"   value="429"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_429" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_429" value="Single Plaits: Large boxes">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[429]" value="1300" placeholder="Enter Amount for Single Plaits: Large boxes" id="fAmount_429" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Single Plaits: Medium boxes<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_430" onchange="check_box_value(this.value);"   value="430"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_430" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_430" value="Single Plaits: Medium boxes">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[430]" value="1300" placeholder="Enter Amount for Single Plaits: Medium boxes" id="fAmount_430" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Single Plaits: Small boxes<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_431" onchange="check_box_value(this.value);"   value="431"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_431" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_431" value="Single Plaits: Small boxes">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[431]" value="30" placeholder="Enter Amount for Single Plaits: Small boxes" id="fAmount_431" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Cornrows: Straight back / Wig braids)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_432" onchange="check_box_value(this.value);"   value="432"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_432" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_432" value="Cornrows: Straight back / Wig braids)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[432]" value="1300" placeholder="Enter Amount for Cornrows: Straight back / Wig braids)" id="fAmount_432" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Straight back: 4 Braids<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_433" onchange="check_box_value(this.value);"   value="433"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_433" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_433" value="Straight back: 4 Braids">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[433]" value="1300" placeholder="Enter Amount for Straight back: 4 Braids" id="fAmount_433" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Straight back: 6 Braids<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_434" onchange="check_box_value(this.value);"   value="434"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_434" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_434" value="Straight back: 6 Braids">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[434]" value="1300" placeholder="Enter Amount for Straight back: 6 Braids" id="fAmount_434" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Straight back: 8 Braids<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_435" onchange="check_box_value(this.value);"   value="435"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_435" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_435" value="Straight back: 8 Braids">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[435]" value="1300" placeholder="Enter Amount for Straight back: 8 Braids" id="fAmount_435" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Fulani Braids: Waist length<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_436" onchange="check_box_value(this.value);"   value="436"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_436" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_436" value="Fulani Braids: Waist length">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[436]" value="50" placeholder="Enter Amount for Fulani Braids: Waist length" id="fAmount_436" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Fulani Braids: Past waist<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_437" onchange="check_box_value(this.value);"   value="437"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_437" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_437" value="Fulani Braids: Past waist">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[437]" value="1300" placeholder="Enter Amount for Fulani Braids: Past waist" id="fAmount_437" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Artificial sisterlocs<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_438" onchange="check_box_value(this.value);"   value="438"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_438" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_438" value="Artificial sisterlocs">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[438]" value="3500" placeholder="Enter Amount for Artificial sisterlocs" id="fAmount_438" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Artificial locs<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_439" onchange="check_box_value(this.value);"   value="439"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_439" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_439" value="Artificial locs">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[439]" value="4500" placeholder="Enter Amount for Artificial locs" id="fAmount_439" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Stitch Feed-Ins: 2 Feed-ins (Dutch)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_440" onchange="check_box_value(this.value);"   value="440"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_440" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_440" value="Stitch Feed-Ins: 2 Feed-ins (Dutch)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[440]" value="1300" placeholder="Enter Amount for Stitch Feed-Ins: 2 Feed-ins (Dutch)" id="fAmount_440" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Stitch Feed-Ins: 4 Feed-ins<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_441" onchange="check_box_value(this.value);"   value="441"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_441" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_441" value="Stitch Feed-Ins: 4 Feed-ins">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[441]" value="1300" placeholder="Enter Amount for Stitch Feed-Ins: 4 Feed-ins" id="fAmount_441" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Stitch Feed-Ins: 6 Feed-ins<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_442" onchange="check_box_value(this.value);"   value="442"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_442" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_442" value="Stitch Feed-Ins: 6 Feed-ins">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[442]" value="1300" placeholder="Enter Amount for Stitch Feed-Ins: 6 Feed-ins" id="fAmount_442" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Stitch Feed-Ins: 8 Feed-ins<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_443" onchange="check_box_value(this.value);"   value="443"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_443" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_443" value="Stitch Feed-Ins: 8 Feed-ins">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[443]" value="1300" placeholder="Enter Amount for Stitch Feed-Ins: 8 Feed-ins" id="fAmount_443" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Stitch Feed-Ins: 10 Feed-ins<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_444" onchange="check_box_value(this.value);"   value="444"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_444" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_444" value="Stitch Feed-Ins: 10 Feed-ins">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[444]" value="50" placeholder="Enter Amount for Stitch Feed-Ins: 10 Feed-ins" id="fAmount_444" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Knotless: Small (Waist length)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_445" onchange="check_box_value(this.value);"   value="445"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_445" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_445" value="Knotless: Small (Waist length)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[445]" value="1400" placeholder="Enter Amount for Knotless: Small (Waist length)" id="fAmount_445" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Knotless: Small (Past waist)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_446" onchange="check_box_value(this.value);"   value="446"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_446" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_446" value="Knotless: Small (Past waist)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[446]" value="100" placeholder="Enter Amount for Knotless: Small (Past waist)" id="fAmount_446" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Knotless: Medium Knotless (Waist length)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_447" onchange="check_box_value(this.value);"   value="447"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_447" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_447" value="Knotless: Medium Knotless (Waist length)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[447]" value="70" placeholder="Enter Amount for Knotless: Medium Knotless (Waist length)" id="fAmount_447" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Knotless: Medium Knotless (Past waist)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_448" onchange="check_box_value(this.value);"   value="448"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_448" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_448" value="Knotless: Medium Knotless (Past waist)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[448]" value="80" placeholder="Enter Amount for Knotless: Medium Knotless (Past waist)" id="fAmount_448" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Knotless: Large (Waist length)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_449" onchange="check_box_value(this.value);"   value="449"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_449" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_449" value="Knotless: Large (Waist length)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[449]" value="40" placeholder="Enter Amount for Knotless: Large (Waist length)" id="fAmount_449" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Knotless: Large (Past waist)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_450" onchange="check_box_value(this.value);"   value="450"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_450" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_450" value="Knotless: Large (Past waist)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[450]" value="50" placeholder="Enter Amount for Knotless: Large (Past waist)" id="fAmount_450" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Wet Haircut with deep nourishing treatmen<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_492" onchange="check_box_value(this.value);"   value="492"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_492" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_492" value="Ladies - Wet Haircut with deep nourishing treatmen">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[492]" value="1500" placeholder="Enter Amount for Ladies - Wet Haircut with deep nourishing treatmen" id="fAmount_492" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Wash, Trim & Rough Dry<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_493" onchange="check_box_value(this.value);"   value="493"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_493" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_493" value="Ladies - Wash, Trim & Rough Dry">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[493]" value="500" placeholder="Enter Amount for Ladies - Wash, Trim & Rough Dry" id="fAmount_493" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Fringe Trim<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_494" onchange="check_box_value(this.value);"   value="494"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_494" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_494" value="Ladies - Fringe Trim">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[494]" value="500" placeholder="Enter Amount for Ladies - Fringe Trim" id="fAmount_494" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Wash & Haircut with Wella mask<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_495" onchange="check_box_value(this.value);"   value="495"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_495" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_495" value="Ladies - Wash & Haircut with Wella mask">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[495]" value="3500" placeholder="Enter Amount for Ladies - Wash & Haircut with Wella mask" id="fAmount_495" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies – Haircut<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_496" onchange="check_box_value(this.value);"   value="496"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_496" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_496" value="Ladies – Haircut">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[496]" value="800" placeholder="Enter Amount for Ladies – Haircut" id="fAmount_496" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Restyle Haircut<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_497" onchange="check_box_value(this.value);"   value="497"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_497" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_497" value="Ladies - Restyle Haircut">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[497]" value="800" placeholder="Enter Amount for Ladies - Restyle Haircut" id="fAmount_497" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Wash & Haircut<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_499" onchange="check_box_value(this.value);"   value="499"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_499" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_499" value="Ladies - Wash & Haircut">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[499]" value="800" placeholder="Enter Amount for Ladies - Wash & Haircut" id="fAmount_499" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Full Head Colouring with Haircut & Blow D<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_500" onchange="check_box_value(this.value);"   value="500"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_500" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_500" value="Ladies - Full Head Colouring with Haircut & Blow D">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[500]" value="2500" placeholder="Enter Amount for Ladies - Full Head Colouring with Haircut & Blow D" id="fAmount_500" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Balayage & Cut/Blowdry<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_501" onchange="check_box_value(this.value);"   value="501"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_501" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_501" value="Ladies - Balayage & Cut/Blowdry">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[501]" value="4500" placeholder="Enter Amount for Ladies - Balayage & Cut/Blowdry" id="fAmount_501" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Wash & Blow Dry<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_502" onchange="check_box_value(this.value);"   value="502"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_502" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_502" value="Ladies - Wash & Blow Dry">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[502]" value="1000" placeholder="Enter Amount for Ladies - Wash & Blow Dry" id="fAmount_502" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Wash, Haircut & Blow Dry<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_503" onchange="check_box_value(this.value);"   value="503"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_503" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_503" value="Ladies - Wash, Haircut & Blow Dry">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[503]" value="1500" placeholder="Enter Amount for Ladies - Wash, Haircut & Blow Dry" id="fAmount_503" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Hair Mask, Haircut & Blow dry<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_504" onchange="check_box_value(this.value);"   value="504"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_504" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_504" value="Ladies - Hair Mask, Haircut & Blow dry">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[504]" value="30" placeholder="Enter Amount for Ladies - Hair Mask, Haircut & Blow dry" id="fAmount_504" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Hair Mask, Haircut & Blow dry<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_505" onchange="check_box_value(this.value);"   value="505"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_505" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_505" value="Ladies - Hair Mask, Haircut & Blow dry">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[505]" value="30" placeholder="Enter Amount for Ladies - Hair Mask, Haircut & Blow dry" id="fAmount_505" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Haircut & Blow Dry<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_506" onchange="check_box_value(this.value);"   value="506"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_506" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_506" value="Ladies - Haircut & Blow Dry">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[506]" value="35" placeholder="Enter Amount for Ladies - Haircut & Blow Dry" id="fAmount_506" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Haircut & Blow Dry with Hair Mask<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_507" onchange="check_box_value(this.value);"   value="507"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_507" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_507" value="Ladies - Haircut & Blow Dry with Hair Mask">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[507]" value="1500" placeholder="Enter Amount for Ladies - Haircut & Blow Dry with Hair Mask" id="fAmount_507" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Dry Haircut with Wet Hair Exclusively<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_508" onchange="check_box_value(this.value);"   value="508"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_508" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_508" value="Ladies - Dry Haircut with Wet Hair Exclusively">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[508]" value="500" placeholder="Enter Amount for Ladies - Dry Haircut with Wet Hair Exclusively" id="fAmount_508" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Hair Colour & highlights - Touch Up<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_589" onchange="check_box_value(this.value);"   value="589"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_589" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_589" value="Ladies' Hair Colour & highlights - Touch Up">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[589]" value="2500" placeholder="Enter Amount for Ladies' Hair Colour & highlights - Touch Up" id="fAmount_589" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Hair Treatments - Botox<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_590" onchange="check_box_value(this.value);"   value="590"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_590" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_590" value="Ladies' Hair Treatments - Botox">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[590]" value="600" placeholder="Enter Amount for Ladies' Hair Treatments - Botox" id="fAmount_590" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Hair Treatments - Half Highlights<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_591" onchange="check_box_value(this.value);"   value="591"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_591" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_591" value="Ladies' Hair Treatments - Half Highlights">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[591]" value="750" placeholder="Enter Amount for Ladies' Hair Treatments - Half Highlights" id="fAmount_591" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Hair Colour - Full Head highlights<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_592" onchange="check_box_value(this.value);"   value="592"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_592" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_592" value="Ladies' Hair Colour - Full Head highlights">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[592]" value="1000" placeholder="Enter Amount for Ladies' Hair Colour - Full Head highlights" id="fAmount_592" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies- Twist Out<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_593" onchange="check_box_value(this.value);"   value="593"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_593" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_593" value="Ladies- Twist Out">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[593]" value="1000" placeholder="Enter Amount for Ladies- Twist Out" id="fAmount_593" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Hair -Ponytail Extensions<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_594" onchange="check_box_value(this.value);"   value="594"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_594" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_594" value="Ladies' Hair -Ponytail Extensions">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[594]" value="700" placeholder="Enter Amount for Ladies' Hair -Ponytail Extensions" id="fAmount_594" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies- Locs Retouch<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_595" onchange="check_box_value(this.value);"   value="595"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_595" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_595" value="Ladies- Locs Retouch">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[595]" value="700" placeholder="Enter Amount for Ladies- Locs Retouch" id="fAmount_595" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies-Twist Braids<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_596" onchange="check_box_value(this.value);"   value="596"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_596" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_596" value="Ladies-Twist Braids">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[596]" value="1500" placeholder="Enter Amount for Ladies-Twist Braids" id="fAmount_596" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies- Stich Lines<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_597" onchange="check_box_value(this.value);"   value="597"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_597" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_597" value="Ladies- Stich Lines">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[597]" value="1700" placeholder="Enter Amount for Ladies- Stich Lines" id="fAmount_597" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Hair - Goddess Braids<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_598" onchange="check_box_value(this.value);"   value="598"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_598" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_598" value="Ladies' Hair - Goddess Braids">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[598]" value="1700" placeholder="Enter Amount for Ladies' Hair - Goddess Braids" id="fAmount_598" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>transformation<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_676" onchange="check_box_value(this.value);"   value="676"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_676" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_676" value="transformation">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[676]" value="10000" placeholder="Enter Amount for transformation" id="fAmount_676" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>knotless <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_689" onchange="check_box_value(this.value);"   value="689"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_689" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_689" value="knotless ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[689]" value="1500" placeholder="Enter Amount for knotless " id="fAmount_689" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>treatment <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_690" onchange="check_box_value(this.value);"   value="690"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_690" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_690" value="treatment ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[690]" value="1200" placeholder="Enter Amount for treatment " id="fAmount_690" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>shoulder dreadlock<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_693" onchange="check_box_value(this.value);"   value="693"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_693" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_693" value="shoulder dreadlock">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[693]" value="1000" placeholder="Enter Amount for shoulder dreadlock" id="fAmount_693" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Locs installation <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_707" onchange="check_box_value(this.value);"   value="707"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_707" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_707" value="Locs installation ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[707]" value="8000" placeholder="Enter Amount for Locs installation " id="fAmount_707" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Natural hair locs<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_708" onchange="check_box_value(this.value);"   value="708"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_708" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_708" value="Natural hair locs">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[708]" value="1400" placeholder="Enter Amount for Natural hair locs" id="fAmount_708" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Stichlines <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_709" onchange="check_box_value(this.value);"   value="709"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_709" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_709" value="Stichlines ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[709]" value="1700" placeholder="Enter Amount for Stichlines " id="fAmount_709" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>butterfly locs<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_710" onchange="check_box_value(this.value);"   value="710"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_710" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_710" value="butterfly locs">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[710]" value="2800" placeholder="Enter Amount for butterfly locs" id="fAmount_710" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Artificial locs- Mid back length<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_715" onchange="check_box_value(this.value);"   value="715"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_715" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_715" value="Artificial locs- Mid back length">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[715]" value="4500" placeholder="Enter Amount for Artificial locs- Mid back length" id="fAmount_715" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Artificial locs- waist length<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_716" onchange="check_box_value(this.value);"   value="716"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_716" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_716" value="Artificial locs- waist length">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[716]" value="5500" placeholder="Enter Amount for Artificial locs- waist length" id="fAmount_716" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Artificial locs- Wax retouch<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_717" onchange="check_box_value(this.value);"   value="717"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_717" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_717" value="Artificial locs- Wax retouch">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[717]" value="700" placeholder="Enter Amount for Artificial locs- Wax retouch" id="fAmount_717" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Artificial locs- Crochet retouch<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_718" onchange="check_box_value(this.value);"   value="718"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_718" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_718" value="Artificial locs- Crochet retouch">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[718]" value="1000" placeholder="Enter Amount for Artificial locs- Crochet retouch" id="fAmount_718" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Weave installation<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_722" onchange="check_box_value(this.value);"   value="722"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_722" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_722" value="Weave installation">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[722]" value="1000" placeholder="Enter Amount for Weave installation" id="fAmount_722" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Knotless bohemian<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_728" onchange="check_box_value(this.value);"   value="728"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_728" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_728" value="Knotless bohemian">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[728]" value="2600" placeholder="Enter Amount for Knotless bohemian" id="fAmount_728" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Coco Twist<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_729" onchange="check_box_value(this.value);"   value="729"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_729" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_729" value="Coco Twist">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[729]" value="3000" placeholder="Enter Amount for Coco Twist" id="fAmount_729" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Extra long passion twist<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_730" onchange="check_box_value(this.value);"   value="730"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_730" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_730" value="Extra long passion twist">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[730]" value="4000" placeholder="Enter Amount for Extra long passion twist" id="fAmount_730" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Short micro nubian<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_731" onchange="check_box_value(this.value);"   value="731"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_731" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_731" value="Short micro nubian">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[731]" value="3000" placeholder="Enter Amount for Short micro nubian" id="fAmount_731" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ghanian lines <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_732" onchange="check_box_value(this.value);"   value="732"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_732" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_732" value="Ghanian lines ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[732]" value="1200" placeholder="Enter Amount for Ghanian lines " id="fAmount_732" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Fancy Lines<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_737" onchange="check_box_value(this.value);"   value="737"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_737" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_737" value="Fancy Lines">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[737]" value="2200" placeholder="Enter Amount for Fancy Lines" id="fAmount_737" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>retouching sister locs<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_738" onchange="check_box_value(this.value);"   value="738"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_738" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_738" value="retouching sister locs">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[738]" value="3000" placeholder="Enter Amount for retouching sister locs" id="fAmount_738" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Spanish Braid<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_739" onchange="check_box_value(this.value);"   value="739"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_739" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_739" value="Spanish Braid">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[739]" value="2700" placeholder="Enter Amount for Spanish Braid" id="fAmount_739" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>fluffy kinky<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_744" onchange="check_box_value(this.value);"   value="744"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_744" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_744" value="fluffy kinky">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[744]" value="270" placeholder="Enter Amount for fluffy kinky" id="fAmount_744" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Natural hair lines<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_751" onchange="check_box_value(this.value);"   value="751"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_751" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_751" value="Natural hair lines">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[751]" value="250" placeholder="Enter Amount for Natural hair lines" id="fAmount_751" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Blowdry<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_752" onchange="check_box_value(this.value);"   value="752"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_752" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_752" value="Blowdry">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[752]" value="200" placeholder="Enter Amount for Blowdry" id="fAmount_752" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Blowdry<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_753" onchange="check_box_value(this.value);"   value="753"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_753" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_753" value="Blowdry">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[753]" value="200" placeholder="Enter Amount for Blowdry" id="fAmount_753" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                    </fieldset>
                                                                                                                <fieldset>
                                                            
                                                            <legend>
                                                                <strong>Beauty Services - Body</strong>
                                                            </legend>

                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Full legs / Half legs waxing<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_419" onchange="check_box_value(this.value);"   value="419"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_419" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_419" value="Ladies - Full legs / Half legs waxing">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[419]" value="2000" placeholder="Enter Amount for Ladies - Full legs / Half legs waxing" id="fAmount_419" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Bikini waxing<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_420" onchange="check_box_value(this.value);"   value="420"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_420" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_420" value="Ladies - Bikini waxing">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[420]" value="3000" placeholder="Enter Amount for Ladies - Bikini waxing" id="fAmount_420" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Underarms / Half Arms<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_421" onchange="check_box_value(this.value);"   value="421"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_421" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_421" value="Ladies - Underarms / Half Arms">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[421]" value="10" placeholder="Enter Amount for Ladies - Underarms / Half Arms" id="fAmount_421" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Men - Body waxing for Men<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_422" onchange="check_box_value(this.value);"   value="422"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_422" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_422" value="Men - Body waxing for Men">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[422]" value="5000" placeholder="Enter Amount for Men - Body waxing for Men" id="fAmount_422" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Leg<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_523" onchange="check_box_value(this.value);"   value="523"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_523" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_523" value="Ladies' Waxing - Leg">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[523]" value="2000" placeholder="Enter Amount for Ladies' Waxing - Leg" id="fAmount_523" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Leg & Brazilian<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_524" onchange="check_box_value(this.value);"   value="524"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_524" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_524" value="Ladies' Waxing - Leg & Brazilian">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[524]" value="3500" placeholder="Enter Amount for Ladies' Waxing - Leg & Brazilian" id="fAmount_524" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Leg & Bikini<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_525" onchange="check_box_value(this.value);"   value="525"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_525" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_525" value="Ladies' Waxing - Leg & Bikini">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[525]" value="4000" placeholder="Enter Amount for Ladies' Waxing - Leg & Bikini" id="fAmount_525" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Leg, Hollywood & Underarm<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_526" onchange="check_box_value(this.value);"   value="526"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_526" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_526" value="Ladies' Waxing - Leg, Hollywood & Underarm">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[526]" value="4500" placeholder="Enter Amount for Ladies' Waxing - Leg, Hollywood & Underarm" id="fAmount_526" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Leg, Brazilian & Underarm<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_527" onchange="check_box_value(this.value);"   value="527"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_527" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_527" value="Ladies' Waxing - Leg, Brazilian & Underarm">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[527]" value="4000" placeholder="Enter Amount for Ladies' Waxing - Leg, Brazilian & Underarm" id="fAmount_527" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Leg, Bikini & Underarm<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_528" onchange="check_box_value(this.value);"   value="528"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_528" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_528" value="Ladies' Waxing - Leg, Bikini & Underarm">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[528]" value="27" placeholder="Enter Amount for Ladies' Waxing - Leg, Bikini & Underarm" id="fAmount_528" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Underarm & Hollywood<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_529" onchange="check_box_value(this.value);"   value="529"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_529" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_529" value="Ladies' Waxing - Underarm & Hollywood">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[529]" value="30" placeholder="Enter Amount for Ladies' Waxing - Underarm & Hollywood" id="fAmount_529" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Underarm & Brazilian<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_530" onchange="check_box_value(this.value);"   value="530"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_530" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_530" value="Ladies' Waxing - Underarm & Brazilian">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[530]" value="27" placeholder="Enter Amount for Ladies' Waxing - Underarm & Brazilian" id="fAmount_530" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Underarm & Bikini<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_531" onchange="check_box_value(this.value);"   value="531"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_531" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_531" value="Ladies' Waxing - Underarm & Bikini">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[531]" value="3500" placeholder="Enter Amount for Ladies' Waxing - Underarm & Bikini" id="fAmount_531" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Full body wax (full leg,full arm,underarm)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_532" onchange="check_box_value(this.value);"   value="532"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_532" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_532" value="Full body wax (full leg,full arm,underarm)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[532]" value="3500" placeholder="Enter Amount for Full body wax (full leg,full arm,underarm)" id="fAmount_532" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Full body with hollywood wax<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_533" onchange="check_box_value(this.value);"   value="533"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_533" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_533" value="Full body with hollywood wax">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[533]" value="6500" placeholder="Enter Amount for Full body with hollywood wax" id="fAmount_533" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Back Facial<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_556" onchange="check_box_value(this.value);"   value="556"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_556" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_556" value="Back Facial">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[556]" value="1500" placeholder="Enter Amount for Back Facial" id="fAmount_556" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Couples Massage<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_563" onchange="check_box_value(this.value);"   value="563"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_563" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_563" value="Couples Massage">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[563]" value="7000" placeholder="Enter Amount for Couples Massage" id="fAmount_563" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Back Massage<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_564" onchange="check_box_value(this.value);"   value="564"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_564" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_564" value="Back Massage">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[564]" value="1500" placeholder="Enter Amount for Back Massage" id="fAmount_564" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Back, Neck & Shoulders Massage<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_565" onchange="check_box_value(this.value);"   value="565"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_565" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_565" value="Back, Neck & Shoulders Massage">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[565]" value="3000" placeholder="Enter Amount for Back, Neck & Shoulders Massage" id="fAmount_565" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Indian Head Massage<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_566" onchange="check_box_value(this.value);"   value="566"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_566" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_566" value="Indian Head Massage">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[566]" value="1500" placeholder="Enter Amount for Indian Head Massage" id="fAmount_566" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Swedish Massage<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_567" onchange="check_box_value(this.value);"   value="567"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_567" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_567" value="Swedish Massage">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[567]" value="3000" placeholder="Enter Amount for Swedish Massage" id="fAmount_567" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Swedish Massage<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_568" onchange="check_box_value(this.value);"   value="568"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_568" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_568" value="Swedish Massage">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[568]" value="3000" placeholder="Enter Amount for Swedish Massage" id="fAmount_568" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Full Body Relaxing Massage<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_569" onchange="check_box_value(this.value);"   value="569"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_569" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_569" value="Full Body Relaxing Massage">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[569]" value="3500" placeholder="Enter Amount for Full Body Relaxing Massage" id="fAmount_569" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Deep Tissue<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_570" onchange="check_box_value(this.value);"   value="570"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_570" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_570" value="Deep Tissue">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[570]" value="3500" placeholder="Enter Amount for Deep Tissue" id="fAmount_570" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Aromatherapy Massage<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_571" onchange="check_box_value(this.value);"   value="571"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_571" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_571" value="Aromatherapy Massage">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[571]" value="3500" placeholder="Enter Amount for Aromatherapy Massage" id="fAmount_571" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Indian Head Massage<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_572" onchange="check_box_value(this.value);"   value="572"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_572" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_572" value="Indian Head Massage">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[572]" value="1500" placeholder="Enter Amount for Indian Head Massage" id="fAmount_572" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Foot Massage<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_573" onchange="check_box_value(this.value);"   value="573"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_573" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_573" value="Foot Massage">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[573]" value="1000" placeholder="Enter Amount for Foot Massage" id="fAmount_573" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Half arm<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_620" onchange="check_box_value(this.value);"   value="620"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_620" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_620" value="Ladies' Waxing - Half arm">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[620]" value="500" placeholder="Enter Amount for Ladies' Waxing - Half arm" id="fAmount_620" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Full arm<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_621" onchange="check_box_value(this.value);"   value="621"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_621" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_621" value="Ladies' Waxing - Full arm">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[621]" value="1000" placeholder="Enter Amount for Ladies' Waxing - Full arm" id="fAmount_621" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Full arm<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_622" onchange="check_box_value(this.value);"   value="622"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_622" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_622" value="Ladies' Waxing - Full arm">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[622]" value="1000" placeholder="Enter Amount for Ladies' Waxing - Full arm" id="fAmount_622" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Half leg<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_623" onchange="check_box_value(this.value);"   value="623"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_623" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_623" value="Ladies' Waxing - Half leg">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[623]" value="1000" placeholder="Enter Amount for Ladies' Waxing - Half leg" id="fAmount_623" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Full leg<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_624" onchange="check_box_value(this.value);"   value="624"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_624" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_624" value="Ladies' Waxing - Full leg">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[624]" value="2000" placeholder="Enter Amount for Ladies' Waxing - Full leg" id="fAmount_624" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - UnderarmDetox<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_625" onchange="check_box_value(this.value);"   value="625"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_625" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_625" value="Ladies' Waxing - UnderarmDetox">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[625]" value="750" placeholder="Enter Amount for Ladies' Waxing - UnderarmDetox" id="fAmount_625" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing -Whole face<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_626" onchange="check_box_value(this.value);"   value="626"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_626" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_626" value="Ladies' Waxing -Whole face">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[626]" value="1000" placeholder="Enter Amount for Ladies' Waxing -Whole face" id="fAmount_626" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Upper lip<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_627" onchange="check_box_value(this.value);"   value="627"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_627" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_627" value="Ladies' Waxing - Upper lip">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[627]" value="200" placeholder="Enter Amount for Ladies' Waxing - Upper lip" id="fAmount_627" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Lower Lip<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_628" onchange="check_box_value(this.value);"   value="628"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_628" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_628" value="Ladies' Waxing - Lower Lip">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[628]" value="200" placeholder="Enter Amount for Ladies' Waxing - Lower Lip" id="fAmount_628" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing -Chin<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_629" onchange="check_box_value(this.value);"   value="629"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_629" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_629" value="Ladies' Waxing -Chin">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[629]" value="300" placeholder="Enter Amount for Ladies' Waxing -Chin" id="fAmount_629" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Vajacial<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_630" onchange="check_box_value(this.value);"   value="630"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_630" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_630" value="Ladies' Waxing - Vajacial">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[630]" value="1500" placeholder="Enter Amount for Ladies' Waxing - Vajacial" id="fAmount_630" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Fab Four<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_631" onchange="check_box_value(this.value);"   value="631"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_631" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_631" value="Ladies' Waxing - Fab Four">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[631]" value="3000" placeholder="Enter Amount for Ladies' Waxing - Fab Four" id="fAmount_631" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>underam waxing<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_711" onchange="check_box_value(this.value);"   value="711"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_711" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_711" value="underam waxing">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[711]" value="350" placeholder="Enter Amount for underam waxing" id="fAmount_711" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Brazilian waxing<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_712" onchange="check_box_value(this.value);"   value="712"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_712" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_712" value="Brazilian waxing">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[712]" value="700" placeholder="Enter Amount for Brazilian waxing" id="fAmount_712" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Body scrub <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_726" onchange="check_box_value(this.value);"   value="726"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_726" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_726" value="Body scrub ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[726]" value="2500" placeholder="Enter Amount for Body scrub " id="fAmount_726" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                    </fieldset>
                                                                                                                <fieldset>
                                                            
                                                            <legend>
                                                                <strong>Beauty Services - Nails</strong>
                                                            </legend>

                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nail Extensions - Acrylic<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_457" onchange="check_box_value(this.value);"   value="457"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_457" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_457" value="Nail Extensions - Acrylic">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[457]" value="3500" placeholder="Enter Amount for Nail Extensions - Acrylic" id="fAmount_457" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nails- Acrylic Refill<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_403" onchange="check_box_value(this.value);"   value="403"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_403" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_403" value="Nails- Acrylic Refill">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[403]" value="2500" placeholder="Enter Amount for Nails- Acrylic Refill" id="fAmount_403" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nails - French Polish<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_511" onchange="check_box_value(this.value);"   value="511"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_511" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_511" value="Nails - French Polish">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[511]" value="800" placeholder="Enter Amount for Nails - French Polish" id="fAmount_511" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Overlays on builder Gel<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_681" onchange="check_box_value(this.value);"   value="681"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_681" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_681" value="Overlays on builder Gel">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[681]" value="1000" placeholder="Enter Amount for Overlays on builder Gel" id="fAmount_681" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Nails Gel Extensions<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_451" onchange="check_box_value(this.value);"   value="451"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_451" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_451" value="Ladies - Nails Gel Extensions">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[451]" value="1000" placeholder="Enter Amount for Ladies - Nails Gel Extensions" id="fAmount_451" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - SPA Pedicure<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_452" onchange="check_box_value(this.value);"   value="452"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_452" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_452" value="Ladies - SPA Pedicure">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[452]" value="30" placeholder="Enter Amount for Ladies - SPA Pedicure" id="fAmount_452" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Shellac Manicure<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_453" onchange="check_box_value(this.value);"   value="453"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_453" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_453" value="Ladies - Shellac Manicure">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[453]" value="25" placeholder="Enter Amount for Ladies - Shellac Manicure" id="fAmount_453" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nail Extensions - Acrylic with Gel colour<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_458" onchange="check_box_value(this.value);"   value="458"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_458" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_458" value="Nail Extensions - Acrylic with Gel colour">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[458]" value="25" placeholder="Enter Amount for Nail Extensions - Acrylic with Gel colour" id="fAmount_458" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nail Extensions - Ombre<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_459" onchange="check_box_value(this.value);"   value="459"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_459" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_459" value="Nail Extensions - Ombre">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[459]" value="27" placeholder="Enter Amount for Nail Extensions - Ombre" id="fAmount_459" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nail Extensions - Pink & White<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_460" onchange="check_box_value(this.value);"   value="460"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_460" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_460" value="Nail Extensions - Pink & White">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[460]" value="35" placeholder="Enter Amount for Nail Extensions - Pink & White" id="fAmount_460" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nail Extensions - Permanent White Tip<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_461" onchange="check_box_value(this.value);"   value="461"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_461" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_461" value="Nail Extensions - Permanent White Tip">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[461]" value="20" placeholder="Enter Amount for Nail Extensions - Permanent White Tip" id="fAmount_461" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nail Extensions - Glitter Powder<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_462" onchange="check_box_value(this.value);"   value="462"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_462" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_462" value="Nail Extensions - Glitter Powder">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[462]" value="27" placeholder="Enter Amount for Nail Extensions - Glitter Powder" id="fAmount_462" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nail Extensions - Toes Acrylic<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_463" onchange="check_box_value(this.value);"   value="463"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_463" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_463" value="Nail Extensions - Toes Acrylic">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[463]" value="28" placeholder="Enter Amount for Nail Extensions - Toes Acrylic" id="fAmount_463" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nail - Get Colour/Shellac<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_464" onchange="check_box_value(this.value);"   value="464"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_464" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_464" value="Nail - Get Colour/Shellac">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[464]" value="18" placeholder="Enter Amount for Nail - Get Colour/Shellac" id="fAmount_464" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nail - Manicure<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_465" onchange="check_box_value(this.value);"   value="465"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_465" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_465" value="Nail - Manicure">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[465]" value="12" placeholder="Enter Amount for Nail - Manicure" id="fAmount_465" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nail - Manicure with Gel colour<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_466" onchange="check_box_value(this.value);"   value="466"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_466" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_466" value="Nail - Manicure with Gel colour">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[466]" value="22" placeholder="Enter Amount for Nail - Manicure with Gel colour" id="fAmount_466" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nail - Pedicure<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_467" onchange="check_box_value(this.value);"   value="467"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_467" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_467" value="Nail - Pedicure">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[467]" value="20" placeholder="Enter Amount for Nail - Pedicure" id="fAmount_467" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nail - Pedicure with Gel colour<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_468" onchange="check_box_value(this.value);"   value="468"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_468" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_468" value="Nail - Pedicure with Gel colour">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[468]" value="28" placeholder="Enter Amount for Nail - Pedicure with Gel colour" id="fAmount_468" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nails - Take off Gel<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_469" onchange="check_box_value(this.value);"   value="469"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_469" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_469" value="Nails - Take off Gel">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[469]" value="5" placeholder="Enter Amount for Nails - Take off Gel" id="fAmount_469" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nails - Take off Acrylic<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_470" onchange="check_box_value(this.value);"   value="470"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_470" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_470" value="Nails - Take off Acrylic">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[470]" value="10" placeholder="Enter Amount for Nails - Take off Acrylic" id="fAmount_470" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nails - Diamond ( x10 )<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_471" onchange="check_box_value(this.value);"   value="471"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_471" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_471" value="Nails - Diamond ( x10 )">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[471]" value="3" placeholder="Enter Amount for Nails - Diamond ( x10 )" id="fAmount_471" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nails - Chrome nails - (Extra)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_472" onchange="check_box_value(this.value);"   value="472"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_472" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_472" value="Nails - Chrome nails - (Extra)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[472]" value="5" placeholder="Enter Amount for Nails - Chrome nails - (Extra)" id="fAmount_472" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nails - Cat eyes - (Extra)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_473" onchange="check_box_value(this.value);"   value="473"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_473" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_473" value="Nails - Cat eyes - (Extra)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[473]" value="3" placeholder="Enter Amount for Nails - Cat eyes - (Extra)" id="fAmount_473" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Nails - Nails art - (From)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_474" onchange="check_box_value(this.value);"   value="474"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_474" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_474" value="Nails - Nails art - (From)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[474]" value="3" placeholder="Enter Amount for Nails - Nails art - (From)" id="fAmount_474" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Spa Pedicure Normal Polish<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_512" onchange="check_box_value(this.value);"   value="512"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_512" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_512" value="Spa Pedicure Normal Polish">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[512]" value="22" placeholder="Enter Amount for Spa Pedicure Normal Polish" id="fAmount_512" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Spa Manicure Normal Polish<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_513" onchange="check_box_value(this.value);"   value="513"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_513" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_513" value="Spa Manicure Normal Polish">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[513]" value="18" placeholder="Enter Amount for Spa Manicure Normal Polish" id="fAmount_513" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>File & Normal Polish - Hands<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_514" onchange="check_box_value(this.value);"   value="514"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_514" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_514" value="File & Normal Polish - Hands">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[514]" value="10" placeholder="Enter Amount for File & Normal Polish - Hands" id="fAmount_514" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>File & Normal Polish - Toes<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_515" onchange="check_box_value(this.value);"   value="515"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_515" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_515" value="File & Normal Polish - Toes">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[515]" value="10" placeholder="Enter Amount for File & Normal Polish - Toes" id="fAmount_515" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Spa Manicure & Pedicure Normal Polish<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_516" onchange="check_box_value(this.value);"   value="516"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_516" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_516" value="Spa Manicure & Pedicure Normal Polish">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[516]" value="42" placeholder="Enter Amount for Spa Manicure & Pedicure Normal Polish" id="fAmount_516" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Gel File & Polish - Toes<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_517" onchange="check_box_value(this.value);"   value="517"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_517" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_517" value="Gel File & Polish - Toes">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[517]" value="18" placeholder="Enter Amount for Gel File & Polish - Toes" id="fAmount_517" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Gel File & Polish - Hands<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_518" onchange="check_box_value(this.value);"   value="518"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_518" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_518" value="Gel File & Polish - Hands">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[518]" value="18" placeholder="Enter Amount for Gel File & Polish - Hands" id="fAmount_518" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Gel Polish Full Manicure<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_519" onchange="check_box_value(this.value);"   value="519"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_519" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_519" value="Gel Polish Full Manicure">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[519]" value="25" placeholder="Enter Amount for Gel Polish Full Manicure" id="fAmount_519" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Gel Polish Full Pedicure<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_520" onchange="check_box_value(this.value);"   value="520"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_520" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_520" value="Gel Polish Full Pedicure">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[520]" value="32" placeholder="Enter Amount for Gel Polish Full Pedicure" id="fAmount_520" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Gel Spa Manicure & Pedicure<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_521" onchange="check_box_value(this.value);"   value="521"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_521" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_521" value="Gel Spa Manicure & Pedicure">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[521]" value="55" placeholder="Enter Amount for Gel Spa Manicure & Pedicure" id="fAmount_521" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Gel File Polish - Hands & Toes<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_522" onchange="check_box_value(this.value);"   value="522"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_522" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_522" value="Gel File Polish - Hands & Toes">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[522]" value="36" placeholder="Enter Amount for Gel File Polish - Hands & Toes" id="fAmount_522" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Gel polish Application<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_680" onchange="check_box_value(this.value);"   value="680"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_680" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_680" value="Gel polish Application">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[680]" value="500" placeholder="Enter Amount for Gel polish Application" id="fAmount_680" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b><br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_679" onchange="check_box_value(this.value);"   value="679"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_679" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_679" value="">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[679]" value="0" placeholder="Enter Amount for " id="fAmount_679" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Overlays on acrylics<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_682" onchange="check_box_value(this.value);"   value="682"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_682" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_682" value="Overlays on acrylics">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[682]" value="1500" placeholder="Enter Amount for Overlays on acrylics" id="fAmount_682" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Full Pedi + Gel<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_683" onchange="check_box_value(this.value);"   value="683"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_683" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_683" value="Full Pedi + Gel">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[683]" value="1500" placeholder="Enter Amount for Full Pedi + Gel" id="fAmount_683" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Overlays on builder gel<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_684" onchange="check_box_value(this.value);"   value="684"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_684" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_684" value="Overlays on builder gel">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[684]" value="1000" placeholder="Enter Amount for Overlays on builder gel" id="fAmount_684" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Gel Application<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_685" onchange="check_box_value(this.value);"   value="685"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_685" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_685" value="Gel Application">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[685]" value="500" placeholder="Enter Amount for Gel Application" id="fAmount_685" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>OVERLAYS WITH BUILDER GEL<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_686" onchange="check_box_value(this.value);"   value="686"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_686" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_686" value="OVERLAYS WITH BUILDER GEL">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[686]" value="1000" placeholder="Enter Amount for OVERLAYS WITH BUILDER GEL" id="fAmount_686" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>tips with gel<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_700" onchange="check_box_value(this.value);"   value="700"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_700" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_700" value="tips with gel">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[700]" value="900" placeholder="Enter Amount for tips with gel" id="fAmount_700" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Gumgel<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_713" onchange="check_box_value(this.value);"   value="713"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_713" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_713" value="Gumgel">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[713]" value="1500" placeholder="Enter Amount for Gumgel" id="fAmount_713" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>overlays - deep powder<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_714" onchange="check_box_value(this.value);"   value="714"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_714" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_714" value="overlays - deep powder">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[714]" value="799" placeholder="Enter Amount for overlays - deep powder" id="fAmount_714" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Normal Nail polish<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_723" onchange="check_box_value(this.value);"   value="723"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_723" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_723" value="Normal Nail polish">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[723]" value="150" placeholder="Enter Amount for Normal Nail polish" id="fAmount_723" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Acrylic dipping <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_735" onchange="check_box_value(this.value);"   value="735"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_735" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_735" value="Acrylic dipping ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[735]" value="1000" placeholder="Enter Amount for Acrylic dipping " id="fAmount_735" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>ombre<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_747" onchange="check_box_value(this.value);"   value="747"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_747" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_747" value="ombre">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[747]" value="999" placeholder="Enter Amount for ombre" id="fAmount_747" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>ombre<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_748" onchange="check_box_value(this.value);"   value="748"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_748" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_748" value="ombre">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[748]" value="999" placeholder="Enter Amount for ombre" id="fAmount_748" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Overlay plus tips<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_749" onchange="check_box_value(this.value);"   value="749"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_749" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_749" value="Overlay plus tips">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[749]" value="999" placeholder="Enter Amount for Overlay plus tips" id="fAmount_749" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Sculpting <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_750" onchange="check_box_value(this.value);"   value="750"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_750" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_750" value="Sculpting ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[750]" value="1499" placeholder="Enter Amount for Sculpting " id="fAmount_750" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b><br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_762" onchange="check_box_value(this.value);"   value="762"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_762" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_762" value="">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[762]" value="0" placeholder="Enter Amount for " id="fAmount_762" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                    </fieldset>
                                                                                                                <fieldset>
                                                            
                                                            <legend>
                                                                <strong>Beauty Services - Face</strong>
                                                            </legend>

                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Bridal Makeup<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_405" onchange="check_box_value(this.value);"   value="405"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_405" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_405" value="Ladies - Bridal Makeup">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[405]" value="50" placeholder="Enter Amount for Ladies - Bridal Makeup" id="fAmount_405" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Bridal & Bridesmaid<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_406" onchange="check_box_value(this.value);"   value="406"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_406" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_406" value="Ladies - Bridal & Bridesmaid">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[406]" value="65" placeholder="Enter Amount for Ladies - Bridal & Bridesmaid" id="fAmount_406" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Face / Upper lip / Eyebrows<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_423" onchange="check_box_value(this.value);"   value="423"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_423" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_423" value="Ladies - Face / Upper lip / Eyebrows">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[423]" value="10" placeholder="Enter Amount for Ladies - Face / Upper lip / Eyebrows" id="fAmount_423" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Microcrystal Dermabrasion<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_424" onchange="check_box_value(this.value);"   value="424"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_424" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_424" value="Ladies - Microcrystal Dermabrasion">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[424]" value="25" placeholder="Enter Amount for Ladies - Microcrystal Dermabrasion" id="fAmount_424" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Black heads cleansing<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_425" onchange="check_box_value(this.value);"   value="425"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_425" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_425" value="Ladies - Black heads cleansing">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[425]" value="35" placeholder="Enter Amount for Ladies - Black heads cleansing" id="fAmount_425" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Wrinkle Correction Instant Lift<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_426" onchange="check_box_value(this.value);"   value="426"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_426" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_426" value="Ladies - Wrinkle Correction Instant Lift">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[426]" value="40" placeholder="Enter Amount for Ladies - Wrinkle Correction Instant Lift" id="fAmount_426" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Lactic Acid peeling<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_427" onchange="check_box_value(this.value);"   value="427"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_427" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_427" value="Ladies - Lactic Acid peeling">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[427]" value="40" placeholder="Enter Amount for Ladies - Lactic Acid peeling" id="fAmount_427" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - C White Lightening Pigmentation<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_428" onchange="check_box_value(this.value);"   value="428"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_428" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_428" value="Ladies - C White Lightening Pigmentation">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[428]" value="40" placeholder="Enter Amount for Ladies - C White Lightening Pigmentation" id="fAmount_428" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Facial - Deep Cleansing with steam<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_554" onchange="check_box_value(this.value);"   value="554"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_554" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_554" value="Facial - Deep Cleansing with steam">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[554]" value="50" placeholder="Enter Amount for Facial - Deep Cleansing with steam" id="fAmount_554" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Facial - Classic<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_555" onchange="check_box_value(this.value);"   value="555"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_555" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_555" value="Facial - Classic">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[555]" value="35" placeholder="Enter Amount for Facial - Classic" id="fAmount_555" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Facial - Basic Dermaplaning<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_557" onchange="check_box_value(this.value);"   value="557"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_557" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_557" value="Facial - Basic Dermaplaning">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[557]" value="45" placeholder="Enter Amount for Facial - Basic Dermaplaning" id="fAmount_557" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Facial - Luxury Dermaplaning<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_558" onchange="check_box_value(this.value);"   value="558"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_558" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_558" value="Facial - Luxury Dermaplaning">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[558]" value="60" placeholder="Enter Amount for Facial - Luxury Dermaplaning" id="fAmount_558" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies -  Ombre<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_605" onchange="check_box_value(this.value);"   value="605"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_605" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_605" value="Ladies -  Ombre">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[605]" value="250" placeholder="Enter Amount for Ladies -  Ombre" id="fAmount_605" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Semi Permanent Makeup (Ombre)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_606" onchange="check_box_value(this.value);"   value="606"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_606" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_606" value="Ladies - Semi Permanent Makeup (Ombre)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[606]" value="250" placeholder="Enter Amount for Ladies - Semi Permanent Makeup (Ombre)" id="fAmount_606" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Semi Permanent Makeup (Powder)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_607" onchange="check_box_value(this.value);"   value="607"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_607" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_607" value="Semi Permanent Makeup (Powder)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[607]" value="250" placeholder="Enter Amount for Semi Permanent Makeup (Powder)" id="fAmount_607" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Semi Permanent Makeup (Combination)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_608" onchange="check_box_value(this.value);"   value="608"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_608" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_608" value="Semi Permanent Makeup (Combination)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[608]" value="275" placeholder="Enter Amount for Semi Permanent Makeup (Combination)" id="fAmount_608" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Semi Permanent Makeup - Colour Refresh<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_609" onchange="check_box_value(this.value);"   value="609"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_609" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_609" value="Semi Permanent Makeup - Colour Refresh">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[609]" value="90" placeholder="Enter Amount for Semi Permanent Makeup - Colour Refresh" id="fAmount_609" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Semi Permanent Makeup - Colour Refresh<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_610" onchange="check_box_value(this.value);"   value="610"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_610" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_610" value="Semi Permanent Makeup - Colour Refresh">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[610]" value="150" placeholder="Enter Amount for Semi Permanent Makeup - Colour Refresh" id="fAmount_610" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Full Face<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_618" onchange="check_box_value(this.value);"   value="618"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_618" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_618" value="Ladies' Waxing - Full Face">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[618]" value="15" placeholder="Enter Amount for Ladies' Waxing - Full Face" id="fAmount_618" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Waxing - Full Face and Neck<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_619" onchange="check_box_value(this.value);"   value="619"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_619" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_619" value="Ladies' Waxing - Full Face and Neck">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[619]" value="18" placeholder="Enter Amount for Ladies' Waxing - Full Face and Neck" id="fAmount_619" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>arabic makeup <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_670" onchange="check_box_value(this.value);"   value="670"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_670" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_670" value="arabic makeup ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[670]" value="60" placeholder="Enter Amount for arabic makeup " id="fAmount_670" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Bridal Makeup<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_671" onchange="check_box_value(this.value);"   value="671"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_671" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_671" value="Bridal Makeup">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[671]" value="100" placeholder="Enter Amount for Bridal Makeup" id="fAmount_671" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Micro Needling<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_678" onchange="check_box_value(this.value);"   value="678"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_678" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_678" value="Ladies - Micro Needling">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[678]" value="6000" placeholder="Enter Amount for Ladies - Micro Needling" id="fAmount_678" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>ladies full makeup<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_720" onchange="check_box_value(this.value);"   value="720"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_720" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_720" value="ladies full makeup">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[720]" value="600" placeholder="Enter Amount for ladies full makeup" id="fAmount_720" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>ladies everyday makeup look<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_721" onchange="check_box_value(this.value);"   value="721"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_721" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_721" value="ladies everyday makeup look">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[721]" value="300" placeholder="Enter Amount for ladies everyday makeup look" id="fAmount_721" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>simple Makeup <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_725" onchange="check_box_value(this.value);"   value="725"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_725" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_725" value="simple Makeup ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[725]" value="1300" placeholder="Enter Amount for simple Makeup " id="fAmount_725" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>makeup - touch up<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_758" onchange="check_box_value(this.value);"   value="758"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_758" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_758" value="makeup - touch up">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[758]" value="1000" placeholder="Enter Amount for makeup - touch up" id="fAmount_758" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Bridesmaid makeup <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_759" onchange="check_box_value(this.value);"   value="759"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_759" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_759" value="Bridesmaid makeup ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[759]" value="2000" placeholder="Enter Amount for Bridesmaid makeup " id="fAmount_759" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Brides mum makeup<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_760" onchange="check_box_value(this.value);"   value="760"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_760" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_760" value="Brides mum makeup">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[760]" value="0" placeholder="Enter Amount for Brides mum makeup" id="fAmount_760" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Professional Make-up <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_763" onchange="check_box_value(this.value);"   value="763"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_763" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_763" value="Professional Make-up ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[763]" value="2000" placeholder="Enter Amount for Professional Make-up " id="fAmount_763" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                    </fieldset>
                                                                                                                <fieldset>
                                                            
                                                            <legend>
                                                                <strong>Beauty Services - Henna</strong>
                                                            </legend>

                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Henna Designs  One hand (one side)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_559" onchange="check_box_value(this.value);"   value="559"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_559" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_559" value="Henna Designs  One hand (one side)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[559]" value="15" placeholder="Enter Amount for Henna Designs  One hand (one side)" id="fAmount_559" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Henna Designs - Simple henna design for any part o<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_560" onchange="check_box_value(this.value);"   value="560"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_560" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_560" value="Henna Designs - Simple henna design for any part o">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[560]" value="25" placeholder="Enter Amount for Henna Designs - Simple henna design for any part o" id="fAmount_560" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Henna Designs - Both hand (one side)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_561" onchange="check_box_value(this.value);"   value="561"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_561" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_561" value="Henna Designs - Both hand (one side)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[561]" value="27" placeholder="Enter Amount for Henna Designs - Both hand (one side)" id="fAmount_561" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Henna Designs - Both hand (both side)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_562" onchange="check_box_value(this.value);"   value="562"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_562" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_562" value="Henna Designs - Both hand (both side)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[562]" value="1000" placeholder="Enter Amount for Henna Designs - Both hand (both side)" id="fAmount_562" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Bridal heena<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_696" onchange="check_box_value(this.value);"   value="696"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_696" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_696" value="Bridal heena">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[696]" value="6000" placeholder="Enter Amount for Bridal heena" id="fAmount_696" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Border legs<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_697" onchange="check_box_value(this.value);"   value="697"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_697" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_697" value="Border legs">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[697]" value="500" placeholder="Enter Amount for Border legs" id="fAmount_697" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Heena stickers <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_698" onchange="check_box_value(this.value);"   value="698"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_698" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_698" value="Heena stickers ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[698]" value="1000" placeholder="Enter Amount for Heena stickers " id="fAmount_698" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Heena stickers <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_699" onchange="check_box_value(this.value);"   value="699"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_699" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_699" value="Heena stickers ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[699]" value="1000" placeholder="Enter Amount for Heena stickers " id="fAmount_699" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                    </fieldset>
                                                                                                                <fieldset>
                                                            
                                                            <legend>
                                                                <strong>Beauty Services - Eyelashes</strong>
                                                            </legend>

                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Classic Eyelashes Extension<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_454" onchange="check_box_value(this.value);"   value="454"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_454" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_454" value="Ladies - Classic Eyelashes Extension">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[454]" value="3000" placeholder="Enter Amount for Ladies - Classic Eyelashes Extension" id="fAmount_454" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - 2-3D Eyelashes Extensions<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_455" onchange="check_box_value(this.value);"   value="455"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_455" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_455" value="Ladies - 2-3D Eyelashes Extensions">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[455]" value="2500" placeholder="Enter Amount for Ladies - 2-3D Eyelashes Extensions" id="fAmount_455" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Volume Eyelashes<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_456" onchange="check_box_value(this.value);"   value="456"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_456" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_456" value="Ladies - Volume Eyelashes">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[456]" value="5500" placeholder="Enter Amount for Ladies - Volume Eyelashes" id="fAmount_456" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyelash Extensions - Classic Semi-Permanent Full S<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_538" onchange="check_box_value(this.value);"   value="538"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_538" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_538" value="Eyelash Extensions - Classic Semi-Permanent Full S">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[538]" value="50" placeholder="Enter Amount for Eyelash Extensions - Classic Semi-Permanent Full S" id="fAmount_538" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyelash Extensions - Classic Semi-Permanent Half S<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_539" onchange="check_box_value(this.value);"   value="539"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_539" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_539" value="Eyelash Extensions - Classic Semi-Permanent Half S">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[539]" value="3000" placeholder="Enter Amount for Eyelash Extensions - Classic Semi-Permanent Half S" id="fAmount_539" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyelash Extensions - Cluster Lashes<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_540" onchange="check_box_value(this.value);"   value="540"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_540" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_540" value="Eyelash Extensions - Cluster Lashes">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[540]" value="2000" placeholder="Enter Amount for Eyelash Extensions - Cluster Lashes" id="fAmount_540" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyelash Extensions - Strip Lashes<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_541" onchange="check_box_value(this.value);"   value="541"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_541" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_541" value="Eyelash Extensions - Strip Lashes">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[541]" value="3500" placeholder="Enter Amount for Eyelash Extensions - Strip Lashes" id="fAmount_541" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyelash Extensions - Infills (From)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_542" onchange="check_box_value(this.value);"   value="542"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_542" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_542" value="Eyelash Extensions - Infills (From)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[542]" value="2500" placeholder="Enter Amount for Eyelash Extensions - Infills (From)" id="fAmount_542" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyelash Extensions - Removal<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_543" onchange="check_box_value(this.value);"   value="543"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_543" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_543" value="Eyelash Extensions - Removal">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[543]" value="1000" placeholder="Enter Amount for Eyelash Extensions - Removal" id="fAmount_543" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyelash Extensions - Russian<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_545" onchange="check_box_value(this.value);"   value="545"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_545" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_545" value="Eyelash Extensions - Russian">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[545]" value="4500" placeholder="Enter Amount for Eyelash Extensions - Russian" id="fAmount_545" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyelash Lift & Tint<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_546" onchange="check_box_value(this.value);"   value="546"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_546" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_546" value="Eyelash Lift & Tint">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[546]" value="3500" placeholder="Enter Amount for Eyelash Lift & Tint" id="fAmount_546" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Lash lift and brow lamination<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_553" onchange="check_box_value(this.value);"   value="553"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_553" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_553" value="Lash lift and brow lamination">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[553]" value="4500" placeholder="Enter Amount for Lash lift and brow lamination" id="fAmount_553" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies - Eyelash extension mink<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_578" onchange="check_box_value(this.value);"   value="578"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_578" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_578" value="Ladies - Eyelash extension mink">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[578]" value="3000" placeholder="Enter Amount for Ladies - Eyelash extension mink" id="fAmount_578" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyelash Extensions - Classic<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_632" onchange="check_box_value(this.value);"   value="632"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_632" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_632" value="Eyelash Extensions - Classic">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[632]" value="3500" placeholder="Enter Amount for Eyelash Extensions - Classic" id="fAmount_632" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyelash Extensions - Hybrid<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_633" onchange="check_box_value(this.value);"   value="633"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_633" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_633" value="Eyelash Extensions - Hybrid">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[633]" value="4500" placeholder="Enter Amount for Eyelash Extensions - Hybrid" id="fAmount_633" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyelash Extensions - Mega Volume<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_634" onchange="check_box_value(this.value);"   value="634"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_634" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_634" value="Eyelash Extensions - Mega Volume">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[634]" value="6500" placeholder="Enter Amount for Eyelash Extensions - Mega Volume" id="fAmount_634" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyelash Extensions - Classic Infill<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_635" onchange="check_box_value(this.value);"   value="635"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_635" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_635" value="Eyelash Extensions - Classic Infill">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[635]" value="2500" placeholder="Enter Amount for Eyelash Extensions - Classic Infill" id="fAmount_635" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyelash Extensions - Hybrid Infill<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_636" onchange="check_box_value(this.value);"   value="636"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_636" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_636" value="Eyelash Extensions - Hybrid Infill">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[636]" value="2500" placeholder="Enter Amount for Eyelash Extensions - Hybrid Infill" id="fAmount_636" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyelash Extensions - Russian Infill<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_637" onchange="check_box_value(this.value);"   value="637"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_637" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_637" value="Eyelash Extensions - Russian Infill">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[637]" value="2500" placeholder="Enter Amount for Eyelash Extensions - Russian Infill" id="fAmount_637" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyelash Extensions - Mega Volume Infill<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_638" onchange="check_box_value(this.value);"   value="638"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_638" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_638" value="Eyelash Extensions - Mega Volume Infill">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[638]" value="2500" placeholder="Enter Amount for Eyelash Extensions - Mega Volume Infill" id="fAmount_638" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Lash - Tweezing<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_677" onchange="check_box_value(this.value);"   value="677"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_677" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_677" value="Lash - Tweezing">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[677]" value="500" placeholder="Enter Amount for Lash - Tweezing" id="fAmount_677" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyebrow shaping<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_736" onchange="check_box_value(this.value);"   value="736"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_736" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_736" value="Eyebrow shaping">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[736]" value="150" placeholder="Enter Amount for Eyebrow shaping" id="fAmount_736" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                    </fieldset>
                                                                                                                <fieldset>
                                                            
                                                            <legend>
                                                                <strong>Beauty Services - Eyebrows</strong>
                                                            </legend>

                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyebrow - Microblading<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_477" onchange="check_box_value(this.value);"   value="477"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_477" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_477" value="Eyebrow - Microblading">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[477]" value="15000" placeholder="Enter Amount for Eyebrow - Microblading" id="fAmount_477" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyebrow - Combination brows (blade & shade)<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_478" onchange="check_box_value(this.value);"   value="478"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_478" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_478" value="Eyebrow - Combination brows (blade & shade)">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[478]" value="500" placeholder="Enter Amount for Eyebrow - Combination brows (blade & shade)" id="fAmount_478" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyebrow - Brow Lamination<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_479" onchange="check_box_value(this.value);"   value="479"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_479" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_479" value="Eyebrow - Brow Lamination">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[479]" value="4000" placeholder="Enter Amount for Eyebrow - Brow Lamination" id="fAmount_479" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyebrow - Threading<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_480" onchange="check_box_value(this.value);"   value="480"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_480" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_480" value="Eyebrow - Threading">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[480]" value="500" placeholder="Enter Amount for Eyebrow - Threading" id="fAmount_480" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyebrow - Upper lip threading<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_481" onchange="check_box_value(this.value);"   value="481"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_481" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_481" value="Eyebrow - Upper lip threading">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[481]" value="500" placeholder="Enter Amount for Eyebrow - Upper lip threading" id="fAmount_481" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Facial Threading - Lower Lip<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_534" onchange="check_box_value(this.value);"   value="534"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_534" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_534" value="Ladies' Facial Threading - Lower Lip">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[534]" value="300" placeholder="Enter Amount for Ladies' Facial Threading - Lower Lip" id="fAmount_534" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Ladies' Facial Threading - Upper Lip<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_535" onchange="check_box_value(this.value);"   value="535"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_535" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_535" value="Ladies' Facial Threading - Upper Lip">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[535]" value="40" placeholder="Enter Amount for Ladies' Facial Threading - Upper Lip" id="fAmount_535" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Microblading Top Up<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_536" onchange="check_box_value(this.value);"   value="536"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_536" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_536" value="Microblading Top Up">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[536]" value="2500" placeholder="Enter Amount for Microblading Top Up" id="fAmount_536" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyebrow Shape & Tint<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_547" onchange="check_box_value(this.value);"   value="547"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_547" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_547" value="Eyebrow Shape & Tint">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[547]" value="750" placeholder="Enter Amount for Eyebrow Shape & Tint" id="fAmount_547" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyebrow & Eyelash Tinting<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_548" onchange="check_box_value(this.value);"   value="548"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_548" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_548" value="Eyebrow & Eyelash Tinting">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[548]" value="750" placeholder="Enter Amount for Eyebrow & Eyelash Tinting" id="fAmount_548" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyebrow Waxing<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_549" onchange="check_box_value(this.value);"   value="549"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_549" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_549" value="Eyebrow Waxing">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[549]" value="650" placeholder="Enter Amount for Eyebrow Waxing" id="fAmount_549" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Brow Lamination without shape and tint<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_551" onchange="check_box_value(this.value);"   value="551"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_551" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_551" value="Brow Lamination without shape and tint">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[551]" value="2500" placeholder="Enter Amount for Brow Lamination without shape and tint" id="fAmount_551" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Brow Lamination with tint and shape<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_552" onchange="check_box_value(this.value);"   value="552"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_552" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_552" value="Brow Lamination with tint and shape">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[552]" value="3500" placeholder="Enter Amount for Brow Lamination with tint and shape" id="fAmount_552" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyebrows - High definition<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_613" onchange="check_box_value(this.value);"   value="613"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_613" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_613" value="Eyebrows - High definition">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[613]" value="2500" placeholder="Enter Amount for Eyebrows - High definition" id="fAmount_613" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Wax and Tint<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_614" onchange="check_box_value(this.value);"   value="614"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_614" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_614" value="Wax and Tint">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[614]" value="1000" placeholder="Enter Amount for Wax and Tint" id="fAmount_614" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyebrows - Henna Tinting<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_617" onchange="check_box_value(this.value);"   value="617"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_617" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_617" value="Eyebrows - Henna Tinting">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[617]" value="500" placeholder="Enter Amount for Eyebrows - Henna Tinting" id="fAmount_617" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>face bleach <br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_687" onchange="check_box_value(this.value);"   value="687"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_687" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_687" value="face bleach ">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[687]" value="500" placeholder="Enter Amount for face bleach " id="fAmount_687" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>Eyebrows shaping<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_724" onchange="check_box_value(this.value);"   value="724"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_724" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_724" value="Eyebrows shaping">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[724]" value="100" placeholder="Enter Amount for Eyebrows shaping" id="fAmount_724" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                            <li style="list-style: outside none none;">
                                                                    <b>eye brow tweezing<br/>
                                                                        <span style="font-size: 12px;"></span>
                                                                    </b>
                                                                    <div class="make-switch" data-on="success" data-off="warning">
                                                                        <input type="checkbox" class="chk" name="vCarType[]" id="vCarType_727" onchange="check_box_value(this.value);"   value="727"/>
                                                                    </div>
                                                                                                                                            <div class="hatchback-search" id="amt1_727" style='display:none;'>
                                                                            <input type="hidden" name="desc" id="desc_727" value="eye brow tweezing">
                                                                                                                                                            <label class="fare_type" style="margin-right:5px;">Ksh </label>	
                                                                                <input class="form-control" type="text" name="fAmount[727]" value="300" placeholder="Enter Amount for eye brow tweezing" id="fAmount_727" maxlength="10"><label class="fare_type">Fixed</label>
                                                                            </div>
                                                                                                                                            </li>
                                                                                                                    </fieldset>
                                                                                                </ul>
                                    </div>
                                    <div class="row" style="display: none;">
                                        <div class="col-lg-12">
                                            <label>Status</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="make-switch" data-on="success" data-off="warning">
                                                <input type="checkbox" name="eStatus" id="eStatus" checked />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="submit" class="btn btn-default" name="submit" id="submit" value="Edit Service" onclick="return check_empty();">
                                            <a href="javascript:void(0);" onclick="reset_form('vehicle_form');" class="btn btn-default">Reset</a>
                                            <a href="vehicles.php" class="btn btn-default back_link">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                                                    </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
            </div>
            <!--END PAGE CONTENT -->
        </div>
        <!--END MAIN WRAPPER -->
        <script>
    var _system_script = 'Driver';
    //Added BY HJ On 05-06-2019 For Auto Hide Message Section Start
    $(document).ready(function () {
        if ($('.alert').html() != '') {
            setTimeout(function () {
                $('.alert').fadeOut();
            }, 4000);
        }
    });
    function hideSetupMessage() {
        $("#footer-new-cube").hide(2000);
    }
    //Added BY HJ On 05-06-2019 For Auto Hide Message Section End
</script>
<script type="text/javascript" src="js/validation/jquery.validate.min.js" ></script>
<script type="text/javascript" src="js/validation/additional-methods.min.js" ></script>
<script type="text/javascript" src="js/form-validation.js" ></script>
<div style="clear:both;"></div>
<div id="footer">
    © FineLooks - 2020</div>
        <script src="../assets/plugins/switch/static/js/bootstrap-switch.min.js"></script>
    </body>
    <!-- END BODY-->
</html>

<script>
                                                $(document).ready(function () {
                                                    var referrer;
                                                    if ($("#previousLink").val() == "") {
                                                        referrer = document.referrer;
                                                        //alert(referrer);
                                                    } else {
                                                        referrer = $("#previousLink").val();
                                                    }
                                                    if (referrer == "") {
                                                        referrer = "vehicles.php";
                                                    } else {
                                                        $("#backlink").val(referrer);
                                                    }
                                                    $(".back_link").attr('href', referrer);
                                                });

                                                function check_box_value(val1)
                                                {
                                                    if ($('#vCarType_' + val1).is(':checked'))
                                                    {
                                                        $("#amt1_" + val1).show();
                                                        $("#fAmount_" + val1).focus();
                                                    } else {
                                                        $("#amt1_" + val1).hide();
                                                    }
                                                }
                                                function check_empty()
                                                {
                                                    var err = 0;
                                                    $("input[type=checkbox]:checked").each(function () {
                                                        var tmp = "fAmount_" + $(this).val();
                                                        var tmp1 = "desc_" + $(this).val();
                                                        var tmp1_val = $("#" + tmp1).val();

                                                        if ($("#" + tmp).val() == "")
                                                        {
                                                            alert('Please Enter Amount for ' + tmp1_val + '.');
                                                            $("#" + tmp).focus();
                                                            err = 1;
                                                            return false;
                                                        }
                                                    });
                                                    if (err == 1)
                                                    {
                                                        return false;
                                                    } else {
                                                        document.vehicle_form.submit();
                                                    }
                                                }
</script>
"""


soup = BeautifulSoup(html_content, 'html.parser')


data = {}

fieldsets = soup.find_all('fieldset')

# for fieldset in fieldsets:
#     # Find the legend within the fieldset
#     legend = fieldset.find('legend')
#     if legend:
#         legend_text = legend.get_text(strip=True)
#         print(f"Legend: {legend_text}")
        
#         # Find all <b> tags within this fieldset
#         b_tags = fieldset.find_all('b')
#         for b_tag in b_tags:
#             b_text = b_tag.get_text(strip=True)
#             amount_input = b_tag.find_next('input', type='text')
#             if amount_input:
#                 amount_value = amount_input.get('value', 'N/A')
#                 print(f" - {b_text} - Amount: {amount_value}")
                
                
# for fieldset in fieldsets:
#     # Find the legend within the fieldset
#     legend = fieldset.find('legend')
#     if legend:
#         legend_text = legend.get_text(strip=True)
#         print(f"Legend: {legend_text}")
        
#         # Find all <b> tags within this fieldset
#         b_tags = fieldset.find_all('b')
#         for b_tag in b_tags:
#             b_text = b_tag.get_text(strip=True)
#             print(f" - {b_text}")
            
#             # Find the input tag within the sibling div
#             sibling_div = b_tag.find_next_sibling('div', class_='hatchback-search')
#             if sibling_div:
#                 amount_input = sibling_div.find('input', type='text')
#                 if amount_input:
#                     amount_value = amount_input.get('value', 'N/A')
#                     print(f"   Amount: {amount_value}")

#                     # Find the checkbox input and extract its ID
#                     checkbox_input = sibling_div.find_previous_sibling('div').find('input', type='checkbox')
#                     if checkbox_input:
#                         id_value = checkbox_input.get('id', 'N/A')
#                         print(f"   ID: {id_value}")

#                         # Extract the number next to vCarType_
#                         id_number = id_value.split('_')[-1]
#                         print(f"   ID Number: {id_number}")

#         print()

for fieldset in fieldsets:
    # Find the legend within the fieldset
    legend = fieldset.find('legend')
    if legend:
        legend_text = legend.get_text(strip=True)
        data[legend_text] = []
        
        # Find all <b> tags within this fieldset
        b_tags = fieldset.find_all('b')
        for b_tag in b_tags:
            b_text = b_tag.get_text(strip=True)
            item_data = {'text': b_text}
            
            # Find the input tag within the sibling div
            sibling_div = b_tag.find_next_sibling('div', class_='hatchback-search')
            if sibling_div:
                amount_input = sibling_div.find('input', type='text')
                if amount_input:
                    amount_value = amount_input.get('value', 'N/A')
                    item_data['amount'] = amount_value

                    # Find the checkbox input and extract its ID
                    checkbox_input = sibling_div.find_previous_sibling('div').find('input', type='checkbox')
                    if checkbox_input:
                        id_value = checkbox_input.get('id', 'N/A')
                        item_data['id'] = id_value

                        # Extract the number next to vCarType_
                        id_number = id_value.split('_')[-1]
                        item_data['id_number'] = id_number
            
            data[legend_text].append(item_data)

#print(data)
# Writing data to a CSV file
csv_filename = 'data.csv'
with open(csv_filename, 'w', newline='') as csvfile:
    fieldnames = ['legend', 'text', 'amount', 'id', 'id_number']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    
    for legend_text, items in data.items():
        for item in items:
            writer.writerow({
                'legend': legend_text,
                'text': item.get('text', ''),
                'amount': item.get('amount', ''),
                'id': item.get('id', ''),
                'id_number': item.get('id_number', '')
            })

print(f"Data written to '{csv_filename}' successfully.")

# Find all modals
# modals = soup.find_all('div', class_='checkbox-group required add-services-hatch car-type-custom')
# legends = soup.find_all('legend')

# print("legends", legends)

# for legend in legends:
#     paragraphs = soup.find_all('b')
#     #print("paragraphs", paragraphs)
#     for b in paragraphs:
#         amounts = soup.find_all('input',class_='form-control')
        
        
#         amount_text = amounts.get_text(strip=True)
#         print("amounts", amount_text)
        

#         b_text = b.get_text(strip=True)
#         #legend_text = legend.get_text(strip=True)
#         print(f"Legend: {b_text}")


# modals = soup.find_all('legend')


# # Extract and print details from each modal
# for modal in modals:
#     header = modal.find('div', class_='modal-header').find('h4').text.strip() if modal.find('div', class_='modal-header') else 'No header'
#     body = modal.find('div', class_='modal-body').find('p').text.strip() if modal.find('div', class_='modal-body') else 'No body'
#     footer_buttons = [btn.text.strip() for btn in modal.find('div', class_='modal-footer').find_all('button')]
#     footer_links = [link.text.strip() for link in modal.find('div', class_='modal-footer').find_all('a')]
    
#     print(f"Modal Header: {header}")
#     print(f"Modal Body: {body}")
#     print(f"Footer Buttons: {', '.join(footer_buttons)}")
#     print(f"Footer Links: {', '.join(footer_links)}")
#     print('-' * 40)