import mysql.connector
from mysql.connector import Error

import csv







def get_driver_services(driver_id, cursor):
    # Your SQL query to retrieve driver services
    query = """
            SELECT * 
            FROM `driver_vehicle` dv 
            LEFT JOIN `service_pro_amount` spa ON dv.iDriverId = spa.iDriverVehicleId 
            WHERE dv.`iDriverId` = %s
        """

    # Execute the query with the given driver_id
    cursor.execute(query, (driver_id,))

    # Fetch the first result
    driver_services = cursor.fetchone()

    return driver_services



def get_partners( cursor):
    # Your SQL query to retrieve driver services
    query = """
            SELECT 
                rd.vEmail,
                rd.iDriverId,
                rd.tRegistrationDate,
                rd.vPhone,
                rd.vCode,
                rd.eStatus,
                rd.eIsFeatured,
                rd.eGender,
                rd.vCountry,
                rd.vServiceLocation,
                rd.vPaymentMethod,
                rd.vBussName,
                rd.vImageProfile,
                rd.tProfileDescription,
                rd.vInstgramPage,
                (
                    SELECT 
                        COUNT(dv.iDriverVehicleId) 
                    FROM 
                        driver_vehicle AS dv 
                    WHERE 
                        dv.iDriverId=rd.iDriverId 
                        AND dv.eStatus != 'Deleted' 
                        AND dv.iMakeId != 0 
                        AND dv.iModelId != 0 
                        AND dv.eType != 'UberX'
                ) AS `count`,
                CONCAT(rd.vName,' ',rd.vLastName) AS driverName
            FROM 
                register_driver rd
            WHERE 
                1=1
        """

    # Execute the query with the given driver_id
    cursor.execute(query)
    results = cursor.fetchall()
    
    
    for row in results:
        print(row)
        
    # Specify the file name for CSV output
    csv_filename = "partners.csv"

    # Open the CSV file in write mode
    with open(csv_filename, mode='w', newline='', encoding='utf-8') as csvfile:
        # Create a CSV writer object
        csv_writer = csv.writer(csvfile)

        # Write header row
        csv_writer.writerow([
            'Email', 'Driver ID', 'Registration Date', 'Phone', 'Code', 'Status', 'Is Featured',
            'Gender', 'Country', 'Service Location', 'Payment Method', 'Business Name',
            'Image Profile', 'Profile Description', 'Instagram Page', 'Vehicle Count', 'Driver Name'
        ])

        # Write each row of data
        for row in results:
            csv_writer.writerow(row)

    print(f"CSV file '{csv_filename}' has been created successfully.")

    
   

try:
    # Establish the connection
    connection = mysql.connector.connect(
        #host='23.235.205.29',        # Example: 'example.com'
        host='23.235.205.29',
        database='finelo6_v5_db',
        user='finelo6_v5_user',
        password='GYS~0}XLkVbx'
    )

    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()
        
        driver_id = 2001
        
        
        
        # driver_services = get_driver_services(driver_id, cursor)
        # print("Driver services:", driver_services[14])
        
        
        
        # partners_information = get_partners(cursor)
        
        print(get_partners(cursor))
        
        # print("partners information", partners_information)


        
        # #driver_id = 2001  # Replace with the driver ID you want to query
        # driver_id = 2100

        
        # #query = "SELECT * FROM `driver_vehicle` dv LEFT JOIN `service_pro_amount` spa ON dv.iDriverId = spa.iDriverVehicleId WHERE dv.`iDriverId` = %s"
        # query = """
        #         SELECT * 
        #         FROM `driver_vehicle` dv 
        #         LEFT JOIN `service_pro_amount` spa ON dv.iDriverId = spa.iDriverVehicleId 
        #         WHERE dv.`iDriverId` = %s
        #     """
        
        # cursor.execute(query, (driver_id,))
        
        
        # driver_services = cursor.fetchone()
        
        # print("driver services", driver_services)
        
        
        #         # Check if the 'fAmount' field is not empty
        # # services_found = False
        # # if driver_services and driver_services.get('fAmount') != '':
        # #     services_found = True

        # Close cursor and connection
        cursor.close()
        connection.close()



except Error as e:
    print("Error while connecting to MySQL", e)

finally:
    if connection.is_connected():
        cursor.close()
        connection.close()
        print("MySQL connection is closed")






