import mysql.connector
from mysql.connector import Error

try:
    # Establish the connection
    connection = mysql.connector.connect(
        #host='23.235.205.29',        # Example: 'example.com'
        host='23.235.205.29',
        database='finelo6_v5_db',
        user='finelo6_v5_user',
        password='GYS~0}XLkVbx'
    )

    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()

        # Execute the SQL statement
        cursor.execute("""
            SELECT 
                rd.vEmail,
                rd.iDriverId,
                rd.tRegistrationDate,
                rd.vPhone,
                rd.vCode,
                rd.eStatus,
                rd.eIsFeatured,
                rd.eGender,
                rd.vCountry,
                rd.vServiceLocation,
                rd.vPaymentMethod,
                rd.vBussName,
                rd.vImageProfile,
                rd.tProfileDescription,
                rd.vInstgramPage,
                (
                    SELECT 
                        COUNT(dv.iDriverVehicleId) 
                    FROM 
                        driver_vehicle AS dv 
                    WHERE 
                        dv.iDriverId=rd.iDriverId 
                        AND dv.eStatus != 'Deleted' 
                        AND dv.iMakeId != 0 
                        AND dv.iModelId != 0 
                        AND dv.eType != 'UberX'
                ) AS `count`,
                CONCAT(rd.vName,' ',rd.vLastName) AS driverName
            FROM 
                register_driver rd
            WHERE 
                1=1
        """)

        # Fetch the results
        results = cursor.fetchall()

        # Print the results with iDriverId as the second column
        for row in results:
            #print(row[1], end=" ")  # iDriverId
            #print(row[0], end=" ")  # vEmail
            
            if (row[1] == 2100):
                
                print(row[2:])          # Other columns

except Error as e:
    print("Error while connecting to MySQL", e)

finally:
    if connection.is_connected():
        cursor.close()
        connection.close()
        print("MySQL connection is closed")
